export const validatePassword = ({ getFieldValue }) => ({
    validator(_, value) {
        if (!value) {
            return Promise.reject(new Error('Por favor ingrese su contraseña.'));
        }

        if (value.length < 8) {
            return Promise.reject(new Error('La contraseña debe tener al menos 8 caracteres.'));
        }

        if (!/[A-Z]/.test(value)) {
            return Promise.reject(new Error('La contraseña debe contener al menos una letra mayúscula.'));
        }

        if (!/[a-z]/.test(value)) {
            return Promise.reject(new Error('La contraseña debe contener al menos una letra minúscula.'));
        }

        if (!/[0-9]/.test(value)) {
            return Promise.reject(new Error('La contraseña debe contener al menos un número.'));
        }
        if (value !== getFieldValue('password-repeat')) {
            return Promise.reject(new Error('Las contraseñas no coinciden.'));
        }

        return Promise.resolve();
    },
});
