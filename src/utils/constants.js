
export const ENV = {
    API_URL: "https://school-theta-wheat.vercel.app/api",
    ENDPOINTS: {
        LOGIN: "auth/signin",
        REGISTER: "Users",
        USER: "Users",
        RESET_PASSWORD: "Users/reset-password-email",
        RESET_PASSWORD_WITH_TOKEN: "Users/reset-password-token",
        STUDENTS: "Users/students",
        DELETE_STUDENTS: "Users",
        ADD_STUDENT: "Users",
        EDIT_STUDENT: "Users",
        EDIT_USER_PROFILE: "Users",
        GET_ALL_USERS: "users",
        ADD_USER: "Users",
        EDIT_USER: "Users",
        DELETE_USER: "Users",
        GET_ALL_CARRERAS:"careers",
        ADD_CARRERAS:"careers",
        DELETE_CARRERAS:"careers",
        EDIT_CARRERAS:"careers",
        MATERIAS:"grades",
        DELETE_MATERIA: "grades",
        ADD_MATERIA: "grades",
        EDIT_MATERIA: "grades",
        PROFESORES: "users/profesor",
        CARRERAS: "careers",
        CALIFICACIONES: "subjects",
        ASISTENCIAS: "assistances",
        GRAFICA1:"graphics/graphic1",
        GRAFICA2:"graphics/graphic2"
    },
    STORAGE: {
        TOKEN:"token"
    }
}