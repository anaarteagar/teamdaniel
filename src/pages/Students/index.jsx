import React, { useState, useEffect } from 'react';
import { Button, notification, Input } from 'antd';
import StudentsTable from '/src/components/Students';
import studentsService from '/src/services/students';
import NavBar from "../../components/NavBar/index.jsx";
import './Students.css';
import AddStudentModal from "../../components/Students/AddStudentsModal.jsx";
import { useAuth } from '../../hooks/useAuth.js';
import { generatePDFEstudiantes } from '../../components/Reportes/estudiantes.js';
import { useTema } from '../../context/TemaContext.jsx';
import { ExportDataMenu } from '../../components/ExportarDatos/ExportDataMenu.jsx';

const Students = () => {

    const tema = useTema();

    const { user } = useAuth();
    const [students, setStudents] = useState([]);
    const [loading, setLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [currentStudent, setCurrentStudent] = useState(null);
    const [filteredStudents, setFilteredStudents] = useState([]);
    const [searchValue, setSearchValue] = useState("");

    const [appliedFilters, setAppliedFilters] = useState({});
    const [appliedSearchValue, setAppliedSearchValue] = useState('');


    useEffect(() => {
        fetchStudents();
    }, []);

    const fetchStudents = async () => {
        try {
            const data = await studentsService.getStudents();
            setStudents(data);
            setFilteredStudents(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar los estudiantes.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };

    const handleDelete = async (id) => {
        try {
            await studentsService.deleteStudent(id);
            const newStudents = students.filter((student) => student._id !== id);
            setStudents(newStudents);
            setFilteredStudents(newStudents);
            notification.success({
                message: 'Estudiante eliminado',
                description: 'El estudiante ha sido eliminado correctamente.',
                placement: 'topRight'
            });
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al eliminar el estudiante.',
                placement: 'topRight'
            });
        }
    };

    const handleEdit = (student) => {
        setCurrentStudent(student);
        setModalVisible(true);
    };

    const handleAdd = () => {
        setCurrentStudent(null);
        setModalVisible(true);
    };

    const handleCreateOrUpdate = async (values) => {
        if (currentStudent) {
            // Editar estudiante
            try {
                const updatedStudent = await studentsService.editStudent(currentStudent._id, values);
                setStudents(students.map((student) => student._id === updatedStudent._id ? updatedStudent : student));
                setFilteredStudents(students.map((student) => student._id === updatedStudent._id ? updatedStudent : student));
                setModalVisible(false);
                notification.success({
                    message: 'Estudiante actualizado',
                    description: 'El estudiante ha sido actualizado correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar el estudiante.',
                    placement: 'topRight'
                });
            }
        } else {
            // Agregar nuevo estudiante
            try {
                const newStudent = await studentsService.addStudent(values);
                setStudents([...students, { ...newStudent, key: newStudent._id }]);
                setFilteredStudents([...students, { ...newStudent, key: newStudent._id }]);
                setModalVisible(false);
                notification.success({
                    message: 'Estudiante agregado',
                    description: 'El estudiante ha sido agregado correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al agregar el estudiante.',
                    placement: 'topRight'
                });
            }
        }
    };

    const filterStudents = (data) => {
        setFilteredStudents(data);
    };

    const handleSearch = () => {

        setAppliedSearchValue(searchValue);

        if (searchValue.trim() === "") {
            filterStudents(students);
        } else {
            const filtered = students.filter(student =>
                Object.values(student).some(value => value.toString().toLowerCase().includes(searchValue.toLowerCase()))
            );
            if (filtered.length === 0) {
                notification.info({
                    message: 'Sin resultados',
                    description: 'No se encontró estudiantes con un dato similar al buscado.',
                    placement: 'topRight'
                });
                filterStudents(students);
            } else {
                setFilteredStudents(filtered);
            }
        }
    };

    const handleInputChange = (e) => {
        setSearchValue(e.target.value);
        if (e.target.value.trim() === "") {
            //filterStudents(students);
        }
    };

    const handleFiltersChange = (filters) => {
        setAppliedFilters(filters);
    };

    return (
        <>
            <NavBar />
            <div className="students-container" style={{ height: 'calc(75vh - 75px)', overflowY: 'auto' }}>
                <h1 className={tema.valor === 'claro' ? "students-table-header" : "students-table-header-oscuro"}>Lista de estudiantes</h1>
                <div className="students-actions">
                    <Button type="primary" onClick={handleAdd} style={{ marginBottom: 16, marginRight: 16 }}>
                        Agregar Estudiante
                    </Button>
                    <Button type="primary" onClick={() => generatePDFEstudiantes(`${user.name} ${user.lastname}`, filteredStudents, appliedFilters, appliedSearchValue)} style={{ marginBottom: 16, marginLeft: 10 }}>
                        Generar reporte
                    </Button>

                    <ExportDataMenu filteredData={filteredStudents} module='students' />

                    <Input
                        className={tema.valor === 'oscuro' ? 'input-dark' : ''}
                        placeholder="Buscar por nombre, apellido o CURP"
                        value={searchValue}
                        onChange={handleInputChange}
                        style={{ width: 300, marginRight: 8 }}
                    />
                    <Button type="primary" onClick={handleSearch}>
                        Buscar
                    </Button>

                </div>
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <StudentsTable
                        students={filteredStudents}
                        onDelete={handleDelete}
                        onEdit={handleEdit}
                        onFilteredDataChange={filterStudents}
                        onFiltersChange={handleFiltersChange}
                        tema={tema}
                    />
                )}
                <AddStudentModal
                    visible={modalVisible}
                    onCreate={handleCreateOrUpdate}
                    onCancel={() => setModalVisible(false)}
                    currentStudent={currentStudent}
                    tema={tema}
                />
            </div>
        </>
    );
};

export default Students;
