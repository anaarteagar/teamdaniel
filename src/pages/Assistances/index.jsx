import React, { useState, useEffect } from 'react';
import { Button, notification, Input } from 'antd';
import AssistancesTable from '../../components/Assistances/index.jsx';
import AddAssistanceModal from '../../components/Assistances/addAssistancesModal.jsx';
import { AssistanceServiceSystem } from '../../services/assistance.js';
import { MateriasServiceSystem } from "../../services/materias.js";
import studentsService from '../../services/students.js';
import NavBar from '../../components/NavBar/index.jsx';
import './Assistances.css';
import { generatePDFAssistances } from '../../components/Reportes/asistencias.js';
import { useAuth } from '../../hooks/useAuth.js';
import { ExportDataMenu } from '../../components/ExportarDatos/ExportDataMenu.jsx';
import { useTema } from '../../context/TemaContext.jsx';

const Assistances = () => {

    const tema = useTema();

    const { user, isProfessor, isServiceSchool, isAlumno } = useAuth();
    const [assistances, setAssistances] = useState([]);
    const [students, setStudents] = useState([]);
    const [graders, setGraders] = useState([]);
    const [loading, setLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [currentAssistance, setCurrentAssistance] = useState(null);
    const [filteredAssistances, setFilteredAssistances] = useState([]);
    const [searchValue, setSearchValue] = useState("");
    const [appliedFilters, setAppliedFilters] = useState({});
    const [appliedSearchValue, setAppliedSearchValue] = useState('');

    useEffect(() => {
        fetchAssistances();
        fetchUsers();
        fetchGraders();
    }, []);

    const fetchAssistances = async () => {
        try {
            const data = await AssistanceServiceSystem.getAssistances();
            setAssistances(data);
            setFilteredAssistances(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las asistencias.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };

    const fetchUsers = async () => {
        try {
            const data = await studentsService.getStudents();
            setStudents(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar los usuarios.',
                placement: 'topRight'
            });
        }
    };

    const fetchGraders = async () => {
        try {
            const data = await MateriasServiceSystem.getMaterias();
            setGraders(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las materias.',
                placement: 'topRight'
            });
        }
    };

    const handleDelete = async (id) => {
        if (isProfessor) {
            try {
                await AssistanceServiceSystem.deleteAssistance(id);
                const newAssistances = assistances.filter((assistance) => assistance._id !== id);
                setAssistances(newAssistances);
                setFilteredAssistances(newAssistances);
                notification.success({
                    message: 'Asistencia eliminada',
                    description: 'La asistencia ha sido eliminada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al eliminar la asistencia.',
                    placement: 'topRight'
                });
            }
        } else {
            notification.info({
                message: 'solo un profesor puede manipular las asistencias',
                description: 'La asistencia solo pueden ser eliminadas por un profesor.',
                placement: 'topRight'
            });
        }
    };

    const handleEdit = (assistance) => {
        if (isProfessor) {
            setCurrentAssistance(assistance);
            setModalVisible(true);
        } else {
            notification.info({
                message: 'solo un profesor puede manipular las asistencias',
                description: 'La asistencia solo pueden ser editadas por un profesor.',
                placement: 'topRight'
            });
        }
    };

    const handleAdd = () => {
        if (isProfessor) {
            setCurrentAssistance(null);
            setModalVisible(true);
        } else {
            notification.info({
                message: 'solo un profesor puede manipular las asistencias',
                description: 'La asistencia solo pueden ser agregadas por un profesor.',
                placement: 'topRight'
            });
        }
    };

    const handleCreateOrUpdate = async (values) => {
        if (currentAssistance) {
            // Editar asistencia
            try {
                const updatedAssistance = await AssistanceServiceSystem.editAssistance(currentAssistance._id, {
                    "professor": user._id,
                    "student": values.student,
                    "mateer": values.mateer,
                    "dia": values.dia,
                    "asistencia": values.asistencia
                });
                setAssistances(assistances.map((assistance) => assistance._id === updatedAssistance._id ? updatedAssistance : assistance));
                setFilteredAssistances(assistances.map((assistance) => assistance._id === updatedAssistance._id ? updatedAssistance : assistance));
                setModalVisible(false);
                notification.success({
                    message: 'Asistencia actualizada',
                    description: 'La asistencia ha sido actualizada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar la asistencia.',
                    placement: 'topRight'
                });
            }
        } else {
            // Agregar nueva asistencia
            try {
                const newAssistance = await AssistanceServiceSystem.addAssistance(
                    {
                        "professor": user._id,
                        "student": values.student,
                        "mateer": values.mateer,
                        "dia": values.dia,
                        "asistencia": values.asistencia
                    }
                );
                setAssistances([...assistances, { ...newAssistance, key: newAssistance._id }]);
                setFilteredAssistances([...assistances, { ...newAssistance, key: newAssistance._id }]);
                setModalVisible(false);
                notification.success({
                    message: 'Asistencia agregada',
                    description: 'La asistencia ha sido agregada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al agregar la asistencia.',
                    placement: 'topRight'
                });
            }
        }
    };

    const filterAssistances = (data) => {
        let filteredData = [...data];

        // Aplicar filtros
        if (appliedFilters.student) {
            filteredData = filteredData.filter(assistance => appliedFilters.student.includes(assistance.student[0].name));
        }
        if (appliedFilters.mateer) {
            filteredData = filteredData.filter(assistance => appliedFilters.mateer.includes(assistance.mateer[0].name));
        }
        if (appliedFilters.professor) {
            filteredData = filteredData.filter(assistance => appliedFilters.professor.includes(assistance.professor[0].name));
        }
        if (appliedFilters.dia && appliedFilters.dia.length === 2) {
            const [startDate, endDate] = appliedFilters.dia[0].map(date => new Date(date)); // Ajuste aquí para manejar arrays anidados
            filteredData = filteredData.filter(assistance => {
                const dia = new Date(assistance.dia);
                return dia >= startDate && dia <= endDate;
            });
        }
        if (appliedFilters.asistencia !== undefined) {
            const asistenciaFilter = appliedFilters.asistencia[0]; // Ajuste aquí para manejar arrays anidados
            filteredData = filteredData.filter(assistance => assistance.asistencia === asistenciaFilter);
        }

        setFilteredAssistances(filteredData);
    };


    const handleSearch = () => {
        setAppliedSearchValue(searchValue);

        if (searchValue.trim() === "") {
            filterAssistances(assistances);
        } else {
            const filtered = assistances.filter(assistance =>
                Object.values(assistance).some(value => {
                    if (Array.isArray(value)) {
                        // Si el valor es una array, verifica cada objeto dentro de la array
                        return value.some(item => item.name.toLowerCase().includes(searchValue.toLowerCase()));
                    } else if (typeof value === 'object') {
                        // Si el valor es un objeto, verifica los campos de ese objeto
                        return Object.values(value).some(v => v.toString().toLowerCase().includes(searchValue.toLowerCase()));
                    } else {
                        return value.toString().toLowerCase().includes(searchValue.toLowerCase());
                    }
                })
            );

            if (filtered.length === 0) {
                notification.info({
                    message: 'Sin resultados',
                    description: 'No se encontró asistencias con un dato similar al buscado.',
                    placement: 'topRight'
                });
            }

            filterAssistances(filtered);
        }
    };


    const handleInputChange = (e) => {
        setSearchValue(e.target.value);
    };

    const handleFiltersChange = (filters) => {
        setAppliedFilters(filters);
    };

    return (
        <>
            <NavBar />
            <div className="assistances-container" style={{ height: 'calc(88vh - 50px)', overflowY: 'auto' }}>
                <h1 className={tema.valor === 'claro' ? "assistances-table-header" : "assistances-table-header-oscuro"}>Lista de Asistencias</h1>
                <div className="assistances-actions">
                    <Button type="primary" onClick={handleAdd} style={{ marginBottom: 16, marginRight: 8 }}>
                        Agregar Asistencia
                    </Button>
                    <Button type="primary" onClick={() => generatePDFAssistances(`${user.name} ${user.lastname}`, filteredAssistances, appliedFilters, appliedSearchValue)} style={{ marginBottom: 16, marginLeft: 10 }}>
                        Generar reporte
                    </Button>

                    <ExportDataMenu filteredData={ filteredAssistances } module='asistencias'  />

                    <Input
                        className={tema.valor === 'oscuro' ? 'input-dark' : ''}
                        placeholder="Buscar"
                        value={searchValue}
                        onChange={handleInputChange}
                        style={{ width: 300, marginRight: 8 }}
                    />
                    <Button type="primary" onClick={handleSearch}>
                        Buscar
                    </Button>
                </div>
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <AssistancesTable
                        assistances={filteredAssistances}
                        onDelete={handleDelete}
                        onEdit={handleEdit}
                        onFilteredDataChange={filterAssistances}
                        onFiltersChange={handleFiltersChange}
                        tema={tema}
                    />
                )}
                <AddAssistanceModal
                    visible={modalVisible}
                    onCreate={handleCreateOrUpdate}
                    onCancel={() => setModalVisible(false)}
                    currentAssistance={currentAssistance}
                    users={students}
                    graders={graders}
                    tema={tema}
                />
            </div>
        </>
    );
};

export default Assistances;
