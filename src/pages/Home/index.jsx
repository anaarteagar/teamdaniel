import {Button} from "antd";
import {useAuth} from "../../hooks/useAuth";
import NavBar from "../../components/NavBar";
import Imagen from "../../components/Form-login/Imagen";
import './Home.css';
import Carrusel from "../../components/carrusel";

const Home = () => {
    const {user,logout} = useAuth();
    return (
        <>
        <NavBar/>
        <div className="div-carrusel">
                <Carrusel/>
            </div>
        </>
     );
}

export default Home;