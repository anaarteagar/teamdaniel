import "./Graphics.css";
import NavBar from "../../components/NavBar/index.jsx";
import React, { useEffect, useState } from "react";
import { notification } from "antd";
import { GraphicsSystem } from "../../services/graphics.js";
import Graphic1 from "../../components/Graphics/index.jsx"; 
import Graphic2 from "../../components/Graphic2/index.jsx";
import { useTema } from "../../context/TemaContext.jsx";

const Graphics = () => {
    const tema = useTema();
    const [data1, setData1] = useState([]);
    const [data2, setData2] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        fetchGraphics();
    }, []);

    const fetchGraphics = async () => {
        try {
            const graphicData1 = await GraphicsSystem.getGraphic1();
            const graphicData2 = await GraphicsSystem.getGraphic2();
            setData1(graphicData1);
            setData2(graphicData2);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las gráficas.',
                placement: 'topRight'
            });
            console.error(error);
        } finally {
            setLoading(false);
        }
    };

    return (
        <>
            <NavBar />
            <div className="graphics-container">
                <h1 className={ tema.valor === 'claro' ? "graphics-header" : "graphics-header-oscuro" }>Gráficas</h1>
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <div className="graphics-wrapper">
                        <div className="graphic-item">
                            <Graphic1 data={data1} />
                        </div>
                        <div className="graphic-item">
                            <Graphic2 data={data2} />
                        </div>
                    </div>
                )}
            </div>
        </>
    );
};

export default Graphics;
