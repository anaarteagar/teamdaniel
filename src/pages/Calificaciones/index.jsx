import "./Calificaciones.css";
import NavBar from "../../components/NavBar/index.jsx";
import { Button, Input, notification } from "antd";
import React, { useEffect, useState } from "react";
import { calificacionesServiceSystem } from "../../services/calificaciones.js";
import CalificacionesTable from "../../components/Calificaciones/index.jsx";
import { MateriasServiceSystem } from "../../services/materias.js";
import studentsService from '/src/services/students';
import { useAuth } from "../../hooks/useAuth";
import CalificacionModal from "../../components/Calificaciones/addCalificacionModal.jsx";
import { generatePDFCalificaciones } from "../../components/Reportes/calificaciones.js";
import { useTema } from "../../context/TemaContext.jsx";
import { ExportDataMenu } from "../../components/ExportarDatos/ExportDataMenu.jsx";

const Calificaciones = () => {

    const tema = useTema();

    const { user, isProfessor, isServiceSchool, isAlumno } = useAuth();
    const [calificaciones, setCalificaciones] = useState([]);
    const [filteredCalificaciones, setFilteredCalificaciones] = useState([]);
    const [materias, setMaterias] = useState([]);
    const [students, setStudents] = useState([]);
    const [matxProfessor, setMatxProfessor] = useState([]);
    const [alumnos, setAlumnos] = useState([]);
    const [loading, setLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [currentCalificacion, setCurrentCalificacion] = useState(null);
    const [searchValue, setSearchValue] = useState("");

    const [appliedFilters, setAppliedFilters] = useState({});
    const [appliedSearchValue, setAppliedSearchValue] = useState('');

    useEffect(() => {
        fetchCalificaciones();
        fetchMaterias();
        fetchStudents();
    }, []);

    const fetchCalificaciones = async () => {
        try {
            const data = await calificacionesServiceSystem.getCalificaciones();
            setCalificaciones(data);
            filterCalificaciones(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las calificaciones.',
                placement: 'topRight'
            });
            console.error(error);
        } finally {
            setLoading(false);
        }
    };



    const fetchMaterias = async () => {
        try {
            const data = await MateriasServiceSystem.getMaterias();
            console.log(data)
            setMaterias(data);
            filterMaterias(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las Materias.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };



    const fetchStudents = async () => {
        try {
            const data = await studentsService.getStudents();
            setAlumnos(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar los estudiantes.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };

    

    const filterCalificaciones = (data) => {
        const filteredData = data.filter(calificacion => calificacion.student && calificacion.student.length > 0 && calificacion.grade && calificacion.grade.length > 0);

        if (isServiceSchool) {
            setFilteredCalificaciones(filteredData);
        } else if (isProfessor) {
            const professorId = user._id.toString();
            const filtered = filteredData.filter(calificacion => {
                const hasProfessor = calificacion.grade.some(gr => {
                    const professorIds = gr.professor.map(id => id.toString());
                    return professorIds.includes(professorId);
                });
                return hasProfessor;
            });
            setFilteredCalificaciones(filtered);
        } else if (isAlumno) {
            const alumnoId = user._id.toString();
            const filtered = filteredData.filter(calificacion => calificacion.student.some(stu => stu._id.toString() === alumnoId));
            setFilteredCalificaciones(filtered);
        } else {
            setFilteredCalificaciones([]);
        }
    };



    const setfilteredMaterias = (data) => {
        console.log("Materias recibidas:", data);
        
            setMatxProfessor(data);
            
        
    };


   

    const filterMaterias = (data) => {
        

        if (isProfessor) {
            
            
            const professorId = user._id;

            console.log(professorId);
            
            setMatxProfessor(null)
            const lasmaterias = data.filter(materia => 
                materia.professor.some(prof => prof._id === professorId)
            );
            
            
            
            
    
            
            setfilteredMaterias(lasmaterias);
            
        }


    };

        
    const handleAddCalificacion = () => {
        setCurrentCalificacion(null);
        setModalVisible(true);
    };

    const handleDeleteCalificacion = async (calificacionId) => {
        try {
            console.log(calificacionId);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al eliminar la calificación.',
                placement: 'topRight'
            });
        }
    };

    const handleEditCalificacion = (calificacion) => {
        setCurrentCalificacion(calificacion);
        setModalVisible(true);
    };

    



    const handleCreateOrUpdate = async (values) => {
        if (currentCalificacion) {
            // Editar carrera

            /*
            try {
                const updatedCarrera = await carreraServiceSystem.editCarreras(currentCareer._id, values);
                setCareers(careers.map((career) => career._id === updatedCarrera._id ? updatedCarrera : career));
                setLoading(true);
                fetchCarreras();
                setLoading(false);
                setModalVisible(false);
                notification.success({
                    message: 'Carrera actualizada',
                    description: 'la carrera ha sido actualizada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar la carrera.',
                    placement: 'topRight'
                });
            }
                */
        } else {
            // Agregar nuevo carrera
            console.log(values);
            try {
                const newCalification = await calificacionesServiceSystem.addCalification(values);
                setLoading(true);
                fetchCalificaciones();
                setLoading(false);
                setModalVisible(false);
                notification.success({
                    message: 'calificacion agregada',
                    description: 'La calificacion ha sido agregada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al agregar la calificacion.',
                    placement: 'topRight'
                });
            }
        }
    };




    const handleSearch = () => {
        
        setAppliedSearchValue(searchValue);
        
        if (searchValue.trim() === "") {
            filterCalificaciones(calificaciones);
        } else {
            const filtered = calificaciones.filter(calificacion =>
                calificacion.student.some(stu => stu.name.toLowerCase().includes(searchValue.toLowerCase()))
            );
            if (filtered.length === 0) {
                notification.info({
                    message: 'Sin resultados',
                    description: 'No se encontró el alumno buscado.',
                    placement: 'topRight'
                });
                filterCalificaciones(calificaciones);
            } else {
                setFilteredCalificaciones(filtered);

                const sinCalificacionFinal = filtered.every(calificacion => !calificacion.finalScore);
                if (sinCalificacionFinal) {
                    notification.warning({
                        message: 'Sin calificación final',
                        description: 'El alumno aún no tiene calificación final.',
                        placement: 'topRight'
                    });
                }
            }
        }
    };

    const handleInputChange = (e) => {
        setSearchValue(e.target.value);
        if (e.target.value.trim() === "") {
            // filterCalificaciones(calificaciones);
        }
    };

    const handleFiltersChange = (filters) => {
        setAppliedFilters(filters);
    };

    return (
        <>
            <NavBar />
            <div className="calificaciones-container" style={{ height: 'calc(75vh - 75px)', overflowY: 'auto' }}>
                <h1 className={ tema.valor === 'claro' ? "calificaciones-table-header" : "calificaciones-table-header-oscuro" }>Lista de Calificaciones</h1>
                {(isProfessor || isServiceSchool) && (
                    <div className="calificaciones-actions">
                        <Button type="primary" onClick={handleAddCalificacion}>
                            Agregar Calificaciones
                        </Button>
                        <Button type="primary" onClick={() => generatePDFCalificaciones(`${user.name} ${user.lastname}`, filteredCalificaciones, appliedFilters, appliedSearchValue)} style={{ marginBottom: 16, marginLeft: 10 }}>
                            Generar reporte
                        </Button>

                        <ExportDataMenu filteredData={ filteredCalificaciones } module='grades' />

                        <Input
                            className={ tema.valor === 'oscuro' ? 'input-dark' : '' }
                            placeholder="Buscar por nombre de estudiante"
                            value={searchValue}
                            onChange={handleInputChange}
                            style={{ width: 300, marginRight: 8 }}
                        />
                        <Button type="primary" onClick={handleSearch}>
                            Buscar
                        </Button>
                    </div>
                )}
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <CalificacionesTable 
                        calificaciones={filteredCalificaciones} 
                        onDelete={handleDeleteCalificacion} 
                        onEdit={handleEditCalificacion}
                        onFilteredDataChange={filterCalificaciones}
                        onFiltersChange={handleFiltersChange}
                        tema={tema}
                    />
                )}

                {/* Cambiar nombre a AddCalificacionModal */}
                {<CalificacionModal
                    visible={modalVisible}
                    onCreate={handleCreateOrUpdate}
                    onCancel={() => setModalVisible(false)}
                    alumnos={alumnos}
                    materias={materias}
                    currentCalification={currentCalificacion}
                    tema={tema}
                /> }
            </div>
        </>
    );
};

export default Calificaciones;
