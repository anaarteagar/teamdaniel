import React, { useState, useEffect } from 'react';
import { Button, notification, Input } from 'antd';
import MateriaTable from '../../components/Materias/index.jsx';
import { MateriasServiceSystem } from '../../services/materias.js';
import NavBar from "../../components/NavBar/index.jsx";
import './Materias.css';
import AddMateriaModal from '../../components/Materias/AddMateriaModal.jsx';
import { useAuth } from '../../hooks/useAuth.js';
import { generatePDFMaterias } from '../../components/Reportes/materias.js';
import { useTema } from '../../context/TemaContext.jsx';
import { ExportDataMenu } from '../../components/ExportarDatos/ExportDataMenu.jsx';

const Materias = () => {


    const tema = useTema();

    const { user } = useAuth();
    const [materias, setMaterias] = useState([]);
    const [loading, setLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [currentMateria, setCurrentMateria] = useState(null);
    const [filteredMaterias, setFilteredMaterias] = useState([]);
    const [searchValue, setSearchValue] = useState("");

    const [appliedFilters, setAppliedFilters] = useState({});
    const [appliedSearchValue, setAppliedSearchValue] = useState('');

    useEffect(() => {
        fetchMaterias();
    }, []);

    const fetchMaterias = async () => {
        try {
            const data = await MateriasServiceSystem.getMaterias();
            console.log(data)
            setMaterias(data);
            setFilteredMaterias(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las Materias.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };

    const handleDelete = async (id) => {
        try {
            await MateriasServiceSystem.deleteMateria(id);
            const newMaterias = materias.filter((materia) => materia._id !== id);
            setMaterias(newMaterias);
            setFilteredMaterias(newMaterias);
            notification.success({
                message: 'Materia eliminada',
                description: 'La materia ha sido eliminada correctamente.',
                placement: 'topRight'
            });
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al eliminar la materia.',
                placement: 'topRight'
            });
        }
    };

    const handleEdit = (materia) => {
        setCurrentMateria(materia);
        setModalVisible(true);
    };

    const handleAdd = () => {
        setCurrentMateria(null);
        setModalVisible(true);
    };

    const handleCreateOrUpdate = async (values) => {
        if (currentMateria) {
            // Editar materia
            try {
                const updatedMateria = await MateriasServiceSystem.editMateria(currentMateria._id, values);
                fetchMaterias();
                setModalVisible(false);
                notification.success({
                    message: 'Materia actualizada',
                    description: 'La materia ha sido actualizada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar la materia.',
                    placement: 'topRight'
                });
            }
        } else {
            // Agregar nueva materia
            try {
                const newMateria = await MateriasServiceSystem.addMateria(values);
                fetchMaterias();
                setModalVisible(false);
                notification.success({
                    message: 'Materia agregada',
                    description: 'La materia ha sido agregada correctamente.',
                    placement: 'topRight'
                });
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al agregar la materia.',
                    placement: 'topRight'
                });
            }
        }
    };

    const filterMaterias = (data) => {
        setFilteredMaterias(data);
    };

    const handleSearch = () => {

        setAppliedSearchValue(searchValue);

        if (searchValue.trim() === "") {
            filterMaterias(materias);
        } else {
            const filtered = materias.filter(materia =>
                Object.values(materia).some(value => value.toString().toLowerCase().includes(searchValue.toLowerCase()))
            );
            if (filtered.length === 0) {
                notification.info({
                    message: 'Sin resultados',
                    description: 'No se encontró materias con un dato similar al buscado.',
                    placement: 'topRight'
                });
                filterMaterias(materias);
            } else {
                setFilteredMaterias(filtered);
            }
        }
    };

    const handleInputChange = (e) => {
        setSearchValue(e.target.value);
        if (e.target.value.trim() === "") {
            //filterMaterias(materias);
        }
    };

    const handleFiltersChange = (filters) => {
        setAppliedFilters(filters);
    };

    return (
        <>
            <NavBar />
            <div className="materias-container" style={{ height: 'calc(75vh - 75px)', overflowY: 'auto' }}>
                <h1 className={tema.valor === 'claro' ? "materias-table-header" : "materias-table-header-oscuro"}>Lista de Materias</h1>
                <div className="materias-actions">
                    <Button type="primary" onClick={handleAdd} style={{ marginBottom: 16, marginRight: 8 }}>
                        Agregar Materia
                    </Button>
                    <Button type="primary" onClick={() => generatePDFMaterias(`${user.name} ${user.lastname}`, filteredMaterias, appliedFilters, appliedSearchValue)} style={{ marginBottom: 16, marginLeft: 10 }}>
                        Generar reporte
                    </Button>

                    <ExportDataMenu filteredData={filteredMaterias} module='materias' />

                    <Input
                        className={tema.valor === 'oscuro' ? 'input-dark' : ''}
                        placeholder="Buscar por nombre de materia"
                        value={searchValue}
                        onChange={handleInputChange}
                        style={{ width: 300, marginRight: 8 }}
                    />
                    <Button type="primary" onClick={handleSearch}>
                        Buscar
                    </Button>
                </div>
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <MateriaTable
                        materias={filteredMaterias}
                        onDelete={handleDelete}
                        onEdit={handleEdit}
                        onFilteredDataChange={filterMaterias}
                        onFiltersChange={handleFiltersChange}
                        tema={tema}
                    />
                )}
                <AddMateriaModal
                    visible={modalVisible}
                    onCreate={handleCreateOrUpdate}
                    onCancel={() => setModalVisible(false)}
                    currentMateria={currentMateria}
                    tema={tema}
                />
            </div>
        </>
    );
};

export default Materias;
