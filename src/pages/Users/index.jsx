import { App } from "antd";
import NavBar from "../../components/NavBar/index.jsx";
import React, { useEffect, useState } from "react";
import { notification, Input, Button } from "antd";
import { userServiceSystem } from "../../services/usersSystem.js";
import UsersTable from "../../components/Users/index.jsx";
import { useAuth } from "../../hooks/useAuth.js";
import './Users.css';
import AddAdminModal from "../../components/Admin/AddAdminModal.jsx";
import { generatePDF } from "../../components/Reportes/usuarios.js";
import { useTema } from "../../context/TemaContext.jsx";
import { ExportDataMenu } from "../../components/ExportarDatos/ExportDataMenu.jsx";

const Users = () => {

    const tema = useTema();

    const { user } = useAuth();
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [currentAdmin, setCurrentAdmin] = useState(null);
    const [searchValue, setSearchValue] = useState("");
    const [appliedFilters, setAppliedFilters] = useState([]);
    const [appliedSearchValue, setAppliedSearchValue] = useState('');

    const [filteredUsers, setFilteredUsers] = useState([]);

    useEffect(() => {
        fetchUsers();
    }, []);

    const fetchUsers = async () => {
        try {
            const data = await userServiceSystem.getUsers();
            setUsers(data);
            setFilteredUsers(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar los usuarios.',
                placement: 'topRight'
            });
            console.error(error);
        } finally {
            setLoading(false);
        }
    };

    const handleAddUser = () => {
        setCurrentAdmin(null);
        setModalVisible(true);
    };

    const handleDeleteUser = async (userId) => {
        if (userId === user._id) {
            notification.error({
                message: 'Error',
                description: 'No puedes eliminar tu propio usuario.',
                placement: 'topRight'
            });
            return;
        }
        try {
            await userServiceSystem.deleteUser(userId);
            const newUsers = users.filter((user) => user._id !== userId);
            setUsers(newUsers);
            setFilteredUsers(newUsers);
            notification.success({
                message: 'Usuario eliminado',
                description: 'El usuario ha sido eliminado correctamente.',
                placement: 'topRight'
            });
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al eliminar al usuario.',
                placement: 'topRight'
            });
        }
    };

    const handleEditUser = (user) => {
        setCurrentAdmin(user);
        setModalVisible(true);
    };

    const handleCreateOrUpdate = async (values) => {
        if (currentAdmin) {
            // Editar usuario
            try {
                const updatedUser = await userServiceSystem.editUser(currentAdmin._id, values);
                // setUsers(users.map((user) => user._id === updatedUser._id ? updatedUser : user));
                // setFilteredUsers(users.map((user) => user._id === updatedUser._id ? updatedUser : user));
                setModalVisible(false);
                notification.success({
                    message: 'Usuario actualizado',
                    description: 'El usuario ha sido actualizado correctamente.',
                    placement: 'topRight'
                });
                fetchUsers();
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar el usuario.',
                    placement: 'topRight'
                });
            }
        } else {
            // Agregar nuevo usuario
            try {
                const newUser = await userServiceSystem.addUser(values);
                // setUsers([...users, { ...newUser, key: newUser._id }]);
                // setFilteredUsers([...users, { ...newUser, key: newUser._id }]);
                setModalVisible(false);
                notification.success({
                    message: 'Usuario agregado',
                    description: 'El usuario ha sido agregado correctamente.',
                    placement: 'topRight'
                });
                fetchUsers();
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al agregar el usuario.',
                    placement: 'topRight'
                });
            }
        }
    };

    const filterUsers = (data) => {
        setFilteredUsers(data);
    };

    const handleSearch = () => {

        setAppliedSearchValue(searchValue);

        if (searchValue.trim() === "") {
            filterUsers(users);
        } else {
            const filtered = users.filter(user =>
                Object.values(user).some(value => value.toString().toLowerCase().includes(searchValue.toLowerCase()))
            );
            if (filtered.length === 0) {
                notification.info({
                    message: 'Sin resultados',
                    description: 'No se encontró usuarios con un dato similar al buscado.',
                    placement: 'topRight'
                });
                filterUsers(users);
            } else {
                setFilteredUsers(filtered);
            }
        }
    };

    const handleInputChange = (e) => {
        setSearchValue(e.target.value);
        if (e.target.value.trim() === "") {
            // filterUsers(users);
        }
    };

    const handleFiltersChange = (filters) => {
        setAppliedFilters(filters);
    };

    return (
        <App>
            <NavBar />
            <div className="users-container" style={{ height: 'calc(75vh - 75px)', overflowY: 'auto' }}>
                <h1 className={tema.valor === 'claro' ? "users-table-header" : "users-table-header-oscuro"}>Lista de Usuarios</h1>
                <div className="usuarios-actions">
                    <Button type="primary" onClick={handleAddUser} style={{ marginBottom: 16, marginRight: 8 }}>
                        Agregar Usuarios
                    </Button>
                    <Button
                        type="primary"
                        onClick={() => generatePDF(`${user.name} ${user.lastname}`, filteredUsers, appliedFilters, appliedSearchValue)}
                        style={{ marginBottom: 16, marginLeft: 10 }}
                    >
                        Generar reporte (PDF)
                    </Button>

                    <ExportDataMenu filteredData={ filteredUsers } module='users'  />
                    
                    <Input
                        className={tema.valor === 'oscuro' ? 'input-dark' : ''}
                        placeholder="Buscar por nombre de usuario,apellidos o correo..."
                        value={searchValue}
                        onChange={handleInputChange}
                        style={{ width: 300, marginRight: 8 }}
                    />
                    <Button type="primary" onClick={handleSearch}>
                        Buscar
                    </Button>
                </div>
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <UsersTable
                        users={filteredUsers}
                        onDelete={handleDeleteUser}
                        onEdit={handleEditUser}
                        onFilteredDataChange={filterUsers}
                        onFiltersChange={handleFiltersChange}
                        tema={tema}
                    />
                )}
                <AddAdminModal
                    visible={modalVisible}
                    onCreate={handleCreateOrUpdate}
                    onCancel={() => setModalVisible(false)}
                    currentAdmin={currentAdmin}
                    tema={tema}
                />
            </div>

        </App>
    );
};

export default Users;
