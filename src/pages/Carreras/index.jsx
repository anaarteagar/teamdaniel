import React, { useState, useEffect } from 'react';
import { Button, notification, Input } from 'antd';
import CareersTable from '/src/components/Carreras';
import { carreraServiceSystem } from '../../services/carrerasSystem.js';
import { MateriasServiceSystem } from '../../services/materias.js';
import NavBar from "../../components/NavBar/index.jsx";
import './Carreras.css';
import AddCarreraModal from "../../components/Carreras/AddCarreraModal.jsx";
import MateriasCarreraModal from '../../components/Carreras/MateriasCarreraModal.jsx';
import { generatePDFCarreras } from '../../components/Reportes/carreras.js';
import { useAuth } from '../../hooks/useAuth.js';
import { useTema } from '../../context/TemaContext.jsx';
import { ExportDataMenu } from '../../components/ExportarDatos/ExportDataMenu.jsx';

const Carreras = () => {


    const tema = useTema();

    const { user } = useAuth();
    const [careers, setCareers] = useState([]);
    const [materias, setMaterias] = useState([]);
    const [matxCareer, setMatxCareer] = useState([]);
    const [loading, setLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalVisiblemateria, setModalVisiblemateria] = useState(false);
    const [currentCareer, setCurrentCareer] = useState(null);
    const [filteredCareers, setFilteredCareers] = useState([]);
    const [searchValue, setSearchValue] = useState("");


    const [appliedFilters, setAppliedFilters] = useState({});
    const [appliedSearchValue, setAppliedSearchValue] = useState('');

    useEffect(() => {
        fetchCarreras();
        fetchMaterias();
    }, []);

    const fetchCarreras = async () => {
        try {
            const data = await carreraServiceSystem.getCarreras();
            setCareers(data);
            setFilteredCareers(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las carreras.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };

    const fetchMaterias = async () => {
        try {
            const data = await MateriasServiceSystem.getMaterias();
            setMaterias(data);
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al cargar las Materias.',
                placement: 'topRight'
            });
        } finally {
            setLoading(false);
        }
    };

    const handleDelete = async (id) => {
        try {
            await carreraServiceSystem.deleteCarreras(id);
            const newCareers = careers.filter((career) => career._id !== id);
            setCareers(newCareers);
            setFilteredCareers(newCareers);
            notification.success({
                message: 'Carrera eliminada',
                description: 'La carrera ha sido eliminada correctamente.',
                placement: 'topRight'
            });
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al eliminar la carrera.',
                placement: 'topRight'
            });
        }
    };

    const handleEdit = (career) => {
        setCurrentCareer(career);
        setModalVisible(true);
    };

    const handleView = (carrera) => {
        setMatxCareer(null);
        const lasmaterias = materias.filter(materia =>
            materia.career.some(car => car._id === carrera._id)
        );
        setCurrentCareer(carrera);
        setMatxCareer(lasmaterias);
        setModalVisiblemateria(true);
    };

    const handleAdd = () => {
        setCurrentCareer(null);
        setModalVisible(true);
    };

    const handleCreateOrUpdate = async (values) => {
        if (currentCareer) {
            try {
                await carreraServiceSystem.editCarreras(currentCareer._id, values);
                setModalVisible(false);
                notification.success({
                    message: 'Carrera actualizada',
                    description: 'La carrera ha sido actualizada correctamente.',
                    placement: 'topRight'
                });
                fetchCarreras();  // Llamada para obtener los datos actualizados
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar la carrera.',
                    placement: 'topRight'
                });
            }
        } else {
            try {
                await carreraServiceSystem.addCarrera(values);
                setModalVisible(false);
                notification.success({
                    message: 'Carrera agregada',
                    description: 'La carrera ha sido agregada correctamente.',
                    placement: 'topRight'
                });
                fetchCarreras();  // Llamada para obtener los datos actualizados
            } catch (error) {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al agregar la carrera.',
                    placement: 'topRight'
                });
            }
        }
    };
    

    const filterCareers = (data) => {
        setFilteredCareers(data);
    };

    const handleSearch = () => {
        
        setAppliedSearchValue(searchValue);
        
        if (searchValue.trim() === "") {
            filterCareers(careers);
        } else {
            const filtered = careers.filter(career =>
                Object.values(career).some(value => value.toString().toLowerCase().includes(searchValue.toLowerCase()))
            );
            if (filtered.length === 0) {
                notification.info({
                    message: 'Sin resultados',
                    description: 'No se encontró carreras con un dato similar al buscado.',
                    placement: 'topRight'
                });
                filterCareers(careers);
            } else {
                setFilteredCareers(filtered);
            }
        }
    };

    const handleInputChange = (e) => {
        setSearchValue(e.target.value);
        if (e.target.value.trim() === "") {
            //filterCareers(careers);
        }
    };

    const handleFiltersChange = (filters) => {
        setAppliedFilters(filters);
    };

    return (
        <>
            <NavBar />
            <div className="carreras-container" style={{ height: 'calc(75vh - 75px)', overflowY: 'auto' }}>
                <h1 className={ tema.valor === 'claro' ? "carreras-table-header" : "carreras-table-header-oscuro" }>Lista de Carreras</h1>
                <div className="carreras-actions">
                    <Button type="primary" onClick={handleAdd} style={{ marginBottom: 16, marginRight: 8 }}>
                        Agregar Carrera
                    </Button>
                    <Button type="primary" onClick={() => generatePDFCarreras(`${user.name} ${user.lastname}`, filteredCareers, appliedFilters, appliedSearchValue)} style={{ marginBottom: 16, marginLeft: 10 }}>
                            Generar reporte
                    </Button>

                    <ExportDataMenu filteredData={ filteredCareers } module='careers'  />
                    
                    <Input
                        className={ tema.valor === 'oscuro' ? 'input-dark' : '' }
                        placeholder="Buscar por nombre de la carrera"
                        value={searchValue}
                        onChange={handleInputChange}
                        style={{ width: 300, marginRight: 8 }}
                    />
                    <Button type="primary" onClick={handleSearch}>
                        Buscar
                    </Button>
                </div>
                {loading ? (
                    <p>Cargando...</p>
                ) : (
                    <CareersTable 
                        careers={filteredCareers} 
                        onDelete={handleDelete} 
                        onEdit={handleEdit} 
                        onView={handleView}
                        onFilteredDataChange={filterCareers}
                        onFiltersChange={handleFiltersChange}
                        tema={tema}     
                    />
                )}
                <AddCarreraModal
                    visible={modalVisible}
                    onCreate={handleCreateOrUpdate}
                    onCancel={() => setModalVisible(false)}
                    currentCareer={currentCareer}
                    tema={tema}
                />
                <MateriasCarreraModal currentCareer={currentCareer} materias={matxCareer} visible={modalVisiblemateria} onCancel={() => setModalVisiblemateria(false)} />
            </div>
            
        </>
    );
};

export default Carreras;
