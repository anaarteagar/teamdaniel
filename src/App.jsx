import './App.css'
import { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './routes/index.jsx';
import { ConfigProvider } from "antd";
import { AuthProvider } from "./context/AuthContext.jsx";
import { useTema } from './context/TemaContext.jsx';

function App() {
    const tema = useTema(); // Obtener tema actual

    useEffect(() => {
        document.body.classList.toggle('dark-mode', tema.valor === 'oscuro');
    }, [tema.valor]);

    return (
        <AuthProvider>
            <ConfigProvider
                theme={{
                    components: {
                        Carousel: {
                            arrowSize: 50
                        },
                    },
                    token: {
                        colorPrimary: tema.colorPrimary,
                        colorText: tema.colorText,
                        colorSecondary: tema.colorSecondary,
                        colorSuccess: tema.colorSuccess,
                        colorInfo: tema.colorInfo,
                    }
                }}
            >
                <BrowserRouter>
                    <AppRoutes></AppRoutes>
                </BrowserRouter>
            </ConfigProvider>
        </AuthProvider>
    )
}
export default App;
