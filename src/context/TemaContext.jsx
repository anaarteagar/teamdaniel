import { useReducer, createContext, useContext, useEffect } from "react";

const TemaContext = createContext(null);
const TemaDispatchContext = createContext(null);

const temas = {
  claro: {
    valor: 'claro',
    backgroundColor: '#f0f0f0',
    colorPrimary: '#666390',
    colorText: '#666390',
    colorSecondary: '#b199dd',
    colorSuccess: '#ffdbf4',
    colorInfo: '#ffdbf4',
  },
  oscuro: {
    valor: 'oscuro',
    backgroundColor: '#121212',
    colorPrimary: '#333',
    colorText: '#fff',
    colorSecondary: '#000',
    colorSuccess: '#000',
    colorInfo: '#000',
  },
};

// Función que obtiene el tema guardado en localStorage o devuelve el tema claro por defecto
const getTemaInicial = () => {
  const temaGuardado = localStorage.getItem('tema');
  return temaGuardado ? temaGuardado : 'claro';
};

export const TemaProvider = ({ children }) => {
  const [state, dispatch] = useReducer(temaReducer, getTemaInicial());

  // Efecto para guardar el tema actual en localStorage cada vez que cambie
  useEffect(() => {
    localStorage.setItem('tema', state);
  }, [state]);

  return (
    <TemaContext.Provider value={temas[state]}>
      <TemaDispatchContext.Provider value={dispatch}>
        {children}
      </TemaDispatchContext.Provider>
    </TemaContext.Provider>
  );
};

const temaReducer = (tema, action) => {
  switch (action.type) {
    case 'cambiar':
      return tema === 'claro' ? 'oscuro' : 'claro';
    default:
      return tema;
  }
};

export const useTema = () => {
  return useContext(TemaContext);
};

export const useTemaDispatch = () => {
  return useContext(TemaDispatchContext);
};
