import React, { useState, useEffect, createContext } from 'react';
import { storageController } from '../services/token';
import { userService } from "../services/users.js";
import { tokenExpired } from "../utils/tokenExpired.js";

export const AuthContext = createContext();

export const AuthProvider = (props) => {
    const { children } = props;
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getSession();
    }, []);

    const getSession = async () => {
        const token = await storageController.getToken();
        if (!token) {
            logout();
            setLoading(false);
            return;
        }
        if (tokenExpired(token)) {
            logout();
            setLoading(false);
            return;
        } else {
            login(token);
        }
        setLoading(false);
    };

    const login = async (token) => {
        try {
            console.log("token obtenido:", token);
            await storageController.setToken(token);
            const response = await userService.getMe(token);
            if (Array.isArray(response) && response.length > 0) {
                setUser(response[0]);
            } else {
                setUser(response);
            }
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    };

    const logout = async () => {
        try {
            await storageController.removeToken();
            setUser(null);
            setLoading(false);
        } catch (error) {
            console.error(error);
            setLoading(false);
        }
    };

    const updateUser = async (data) => {
        try {
            if (user && user._id) {
                const updatedUser = await userService.updateUser(data, user._id);
                setUser(updatedUser);
                return updatedUser;
            } else {
                throw new Error('User ID is not available');
            }
        } catch (error) {
            console.error(error);
            throw error;
        }
    };

    const isProfessor = user?.roles?.some(role => role.name === 'profesor');
    const isAlumno = user?.roles?.some(role => role.name === 'alumno');
    const isServiceSchool = user?.roles?.some(role => role.name === 'servicios_escolares');

    const data = {
        user,
        login,
        logout,
        updateUser,
        isProfessor,
        isServiceSchool,
        isAlumno,
    };

    if (loading) return null;

    return (
        <AuthContext.Provider value={data}>
            {children}
        </AuthContext.Provider>
    );
};

