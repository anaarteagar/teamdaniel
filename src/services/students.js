import axios from 'axios';
import { ENV } from "../utils/constants";
import {storageController} from "./token.js";

const getStudents = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.STUDENTS}`, {
        headers: {
            'x-access-token': token,
        }
    });
    return response.data;
} catch (error) {
    console.error('Error fetching students:', error);
    throw error;
}
};
const deleteStudent = async (key) => {
    try {
        const token = storageController.getToken();
        const response = await axios.delete(`${ENV.API_URL}/${ENV.ENDPOINTS.DELETE_STUDENTS}/${key}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error deleting student:', error);
        throw error;
    }
};
const addStudent = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.ADD_STUDENT}`, data, {
        headers: {
            'x-access-token': token,
        }
    });
    return response.data.user;
} catch (error) {
    console.error('Error adding student:', error);
    throw error;
}
};
const editStudent = async (key, data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.EDIT_STUDENT}/${key}`, data, {
            headers: {
                'x-access-token': token,
            }
    });
    return response.data.user;
} catch (error) {
    console.error('Error editing student:', error);
    throw error;
}
};

export default { getStudents, deleteStudent, addStudent, editStudent };