import axios from 'axios';
import { ENV } from "../utils/constants";
import { storageController } from "./token.js";

const getMaterias = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.MATERIAS}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching materials:', error);
        throw error;
    }
};


const deleteMateria = async (key) => {
    try {
        const token = storageController.getToken();
        const response = await axios.delete(`${ENV.API_URL}/${ENV.ENDPOINTS.DELETE_MATERIA}/${key}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error deleting materia:', error);
        throw error;
    }
};
const addMateria = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.ADD_MATERIA}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error adding materia:', error);
        throw error;
    }
};
const editMateria = async (key, data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.EDIT_MATERIA}/${key}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error editing materia:', error);
        throw error;
    }
};




export const MateriasServiceSystem = { getMaterias, deleteMateria, addMateria, editMateria };