import axios from 'axios';
import { ENV } from "../utils/constants";
import { storageController } from "./token.js";

const getCarreras = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.CARRERAS}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching carreras:', error);
        throw error;
    }
};




export const carrerasServiceSystem = { getCarreras };