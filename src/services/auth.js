import axios from 'axios';
import {ENV} from "../utils/constants";
import { storageController } from '../services/token';
const register = async (name, password, email, lastname) => {
    return await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.REGISTER}`, {
        name,
        password,
        email,
        lastname,
        roles: ['servicios_escolares']
    })
};
const loginF = async (email, password) => {
    return await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.LOGIN}`, {
        email,
        password
    })

    
};

const loginA = async (studentId, CURP) => {
    return await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.LOGIN}`, {
        studentId,
        CURP
    })

    
};
const resetPassword = async (email, password) => {
    return await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.RESET_PASSWORD}`, {
        email,
        password
    });
};

export const resetPasswordWithToken = async (password) => {
    
    const token = storageController.getToken();
    console.log(token)
    return await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.RESET_PASSWORD_WITH_TOKEN}`,
         {password},
        {
            headers: {
                'x-access-token': token
            }
        }
    );
}
export default {register, loginF, resetPassword, resetPasswordWithToken, loginA};

