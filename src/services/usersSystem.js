import axios from 'axios';
import { ENV } from "../utils/constants";
import {storageController} from "./token.js";

const getUsers = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.GET_ALL_USERS}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching students:', error);
        throw error;
    }
};
const addUser = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.ADD_USER}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data.user;
    } catch (error) {
        console.error('Error adding student:', error);
        throw error;
    }
};
const editUser = async (key, data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.EDIT_USER}/${key}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data.user;
    } catch (error) {
        console.error('Error editing student:', error);
        throw error;
    }
};


const deleteUser = async (key) => {
    try {
        const token = storageController.getToken();
        const response = await axios.delete(`${ENV.API_URL}/${ENV.ENDPOINTS.DELETE_USER}/${key}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error deleting user:', error);
        throw error;
    }
};

export const userServiceSystem = {
    getUsers,
    addUser,
    editUser,
    deleteUser
}