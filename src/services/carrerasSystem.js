import axios from 'axios';
import { ENV } from "../utils/constants.js";
import {storageController} from "./token.js";

const getCarreras = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.GET_ALL_CARRERAS}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching careers:', error);
        throw error;
    }
};
const addCarrera = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.ADD_CARRERAS}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data.career;
    } catch (error) {
        console.error('Error adding career:', error);
        throw error;
    }
};
const deleteCarreras = async (key) => {
    try {
        const token = storageController.getToken();
        const response = await axios.delete(`${ENV.API_URL}/${ENV.ENDPOINTS.DELETE_CARRERAS}/${key}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error deleting career:', error);
        throw error;
    }
}
const editCarreras = async (key, data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.EDIT_CARRERAS}/${key}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        console.log("carrera: ",response.data);
        return response.data;
    } catch (error) {
        console.error('Error editing career:', error);
        throw error;
    }
}


export const carreraServiceSystem = {
    getCarreras,
    addCarrera,
    deleteCarreras,
    editCarreras
}