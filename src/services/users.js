import {jwtDecode} from "jwt-decode";
import {ENV} from "../utils/constants";
import {authFetch} from "../utils/authFetch";
import {storageController} from "./token";
import axios from 'axios';
const getMe = async (token) => {
    try {
        const decoded = jwtDecode(token);
        const userId = decoded.id;
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.USER}/${userId}`;
        const response = await authFetch(url);
        return response.json();
    } catch (error) {
        console.log(error);
    }
};

const updateUser = async (data, id) => {
    try {
        const token = await storageController.getToken();
        const response = await axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.EDIT_USER_PROFILE}/${id}`, data, {
            headers: {
                'x-access-token': token,
            }
        });
        if (response.data) {
            return response.data.user;
        } else {
            throw new Error('No data in response');
        }
    } catch (error) {
        console.error('Error updating user:', error);
        throw error;
    }
};

export const userService = {getMe, updateUser};