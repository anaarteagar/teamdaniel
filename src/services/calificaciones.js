import axios from 'axios';
import { ENV } from "../utils/constants";
import { storageController } from "./token.js";

const getCalificaciones = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.CALIFICACIONES}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching calificaciones:', error);
        throw error;
    }
}



const addCalification = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.CALIFICACIONES}`, {
            "student": [
                                    data.student
                                  ],
                                  "parcial": data.parcial,
                                  "grado": data.grado,
                                  "grade": [
                                    data.grade
                                  ],
                                  "subject": data.subject}, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error adding calification:', error);
        throw error;
    }
};

export const calificacionesServiceSystem = { getCalificaciones,addCalification };
