import axios from 'axios';
import { ENV } from "../utils/constants";
import {storageController} from "./token.js";


const addAdmin = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.ADD_ADMIN}`, data, {
        headers: {
            'x-access-token': token,
        }
    });
    return response.data.user;
} catch (error) {
    console.error('Error adding admin:', error);
    throw error;
}
};


export default { addAdmin };