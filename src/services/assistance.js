import axios from 'axios';
import { ENV } from "../utils/constants";
import {storageController} from "./token.js";

const getAssistances = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.ASISTENCIAS}`, {
        headers: {
            'x-access-token': token,
        }
    });
    return response.data;
} catch (error) {
    console.error('Error fetching assistances:', error);
    throw error;
}
};
const deleteAssistance = async (key) => {
    try {
        const token = storageController.getToken();
        const response = await axios.delete(`${ENV.API_URL}/${ENV.ENDPOINTS.ASISTENCIAS}/${key}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error deleting assistance:', error);
        throw error;
    }
};
const addAssistance = async (data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.ASISTENCIAS}`, data, {
        headers: {
            'x-access-token': token,
        }
    });
    return response.data.savedAssistance;
} catch (error) {
    console.error('Error adding Assistance:', error);
    throw error;
}
};
const editAssistance = async (key, data) => {
    try {
        const token = storageController.getToken();
        const response = await axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.ASISTENCIAS}/${key}`, data, {
            headers: {
                'x-access-token': token,
            }
    });
    return response.data.assistanceToUpdate;
} catch (error) {
    console.error('Error editing Assistance:', error);
    throw error;
}
};

export const AssistanceServiceSystem = { getAssistances, deleteAssistance, addAssistance, editAssistance };