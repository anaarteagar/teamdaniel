import axios from 'axios';
import { ENV } from "../utils/constants";
import { storageController } from "./token.js";

const getProfesores = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.PROFESORES}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching profesores:', error);
        throw error;
    }
};




export const profesoresServiceSystem = { getProfesores };