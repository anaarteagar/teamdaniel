import axios from 'axios';
import { ENV } from "../utils/constants";
import { storageController } from "./token.js";

const getGraphic1 = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.GRAFICA1}`, {
            headers: {
                'x-access-token': token,
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching materials:', error);
        throw error;
    }
};

const getGraphic2 = async () => {
    try {
        const token = storageController.getToken();
        const response = await axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.GRAFICA2}`, {
            headers: {
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching materials:', error);
        throw error;
    }
};

export const GraphicsSystem = { getGraphic1, getGraphic2 };