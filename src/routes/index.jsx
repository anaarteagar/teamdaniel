
import {useRoutes} from "react-router-dom";
import Login from "../pages/Login/Login.jsx";
import Home from "../pages/Home/index.jsx";
import Register from "../pages/Register/index.jsx";
import {useAuth} from "../hooks/useAuth.js";
import ForgotPassword from '/src/components/ForgotPassword';
import Students from "../pages/Students/index.jsx";
import Alumnos from "../pages/Alumnos/index.jsx";
import Users from "../pages/Users/index.jsx";
import Carreras from "../pages/Carreras/index.jsx";
import Materias from "../pages/Materias/index.jsx";
import Calificaciones from "../pages/Calificaciones/index.jsx";
import Assistances from "../pages/Assistances/index.jsx";
import Graphics from "../pages/Graphics/index.jsx";

const AppRoutes = () => {
    const { user, isProfessor, isServiceSchool, isAlumno } = useAuth();

    let routes = useRoutes([
        { path: "/", element: user ? <Home /> : <Login /> },
        { path: "/login", element: <Login /> },
        { path: "/register", element: <Register /> },
        {path:"/alumnos", element: <Alumnos/>},
        { path: "/forgot-password", element: <ForgotPassword /> },
        { path: "/students", element: user ? <Students /> : <Login /> },
        { path: "/users", element: isServiceSchool ? <Users /> : <Login /> },
        { path: "/carreras", element: user ? <Carreras /> : <Login /> },
        { path: "/materias", element: user ? <Materias /> : <Login /> },
        { path: "/calificaciones", element: (isProfessor || isServiceSchool || isAlumno) ? <Calificaciones /> : <Login /> },
        { path: "/asistencias", element: (isProfessor || isServiceSchool ) ? <Assistances /> : <Login /> },
        { path: "/graphics", element: (isProfessor || isServiceSchool ) ? <Graphics /> : <Login /> },
    ]);

    return routes;
};

export default AppRoutes