import React from 'react';
import { Carousel } from 'antd';
import Imagen from '../../assets/logo.png';
import ImagenMaterias from '../../assets/materias.png';
import ImagenEstudiantes from '../../assets/klipartz.com.png';
import Imagencalificacion from '../../assets/pngwing.com.png';
import ImagenGrafica from '../../assets/Tomardecisionesbasadasendatos.png';
import './carrusel.css';


const Carrusel = () => {
  return (
    <Carousel autoplay arrows infinite={true}>
      <div className="carousel-slide">
        <img
          src={ImagenEstudiantes}
          className="carousel-image"
        />
        <div className="carousel-content">
          <h3>Gestión de Estudiantes</h3>
          <p>Administra y consulta la información de los estudiantes fácilmente.</p>
        </div>
      </div>
      <div className="carousel-slide">
        <img
          src={ImagenMaterias}
          className="carousel-image"
        />
        <div className="carousel-content">
          <h3>Materias Impartidas</h3>
          <p>Visualiza y gestiona las materias que estás impartiendo.</p>
        </div>
      </div>
      <div className="carousel-slide">
        <img
          src={Imagencalificacion}
          className="carousel-image"
        />
        <div className="carousel-content">
          <h3>Calificaciones</h3>
          <p>Registra y consulta las calificaciones de los estudiantes.</p>
        </div>
      </div>
      <div className="carousel-slide">
        <img
          src={ImagenGrafica}
          className="carousel-image"
        />
        <div className="carousel-content">
          <h3>Reportes y Análisis</h3>
          <p>Genera reportes detallados y analiza el rendimiento académico.</p>
        </div>
      </div>
    </Carousel>
  );
};

export default Carrusel;
