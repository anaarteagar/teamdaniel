import React, { useEffect } from 'react';
import { Modal, Form, Input } from 'antd';

const MateriasCarreraModal = ({ visible, onCancel, currentCareer,materias }) => {
    
    

    

    //console.log(materias);


    


    return (
        <Modal
            visible={visible}
            title={`Materias de la carrera: ${currentCareer ? currentCareer.name : ''}`}
            cancelText="Cerrar"
            onCancel={onCancel}
            okText={"cerrar"}
            onOk={onCancel}
            
            
        >

         <h5>Materias</h5>   
         <div>
                {materias.length > 0 ? (
                    materias.map((materia) => (
                        <p key={materia._id}>{materia.name}</p>
                    ))
                ) : (
                    <p>No hay materias disponibles para esta carrera.</p>
                )}
            </div>
                

        </Modal>
    );
};

export default MateriasCarreraModal;
