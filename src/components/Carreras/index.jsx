import { Button, Popconfirm, Table } from "antd";
import { DeleteOutlined, EditOutlined, EyeFilled } from "@ant-design/icons";
import React from "react";

const CareersTable = ({ careers, onDelete, onEdit, onView, onFilteredDataChange, onFiltersChange, tema }) => {

    const uniqueValues = (array, key) => {
        return [...new Set(array.map(item => item[key]))];
    };

    const nameFilters = uniqueValues(careers, 'name').map(name => ({ text: name, value: name }));

    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'name',
            key: 'name',
            render: name => name || "La Carrera no tiene nombre",
            filters: nameFilters,
            onFilter: (value, record) => record.name.includes(value),
            filterSearch: true,
            width: '70%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Acciones',
            key: 'actions',
            className: tema.valor === 'oscuro' ? 'dark-column' : '', // Condicional para clase personalizada
            render: (text, record) => (
                <span>
                    <Button
                        type="primary"
                        icon={<EditOutlined />}
                        onClick={() => onEdit(record)}
                        style={{ marginRight: 8 }}
                    >
                        Editar
                    </Button>
                    <Popconfirm
                        title="¿Estás seguro de eliminar esta carrera?"
                        onConfirm={() => onDelete(record._id)}
                        okText="Sí"
                        cancelText="No"
                        overlayClassName={tema.valor === 'oscuro' ? 'dark-popconfirm' : ''} // Aplicar estilo de Popconfirm en modo oscuro
                    >
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Eliminar
                        </Button>
                    </Popconfirm>

                    <Button
                        type="primary"
                        icon={<EyeFilled />}
                        onClick={() => onView(record)}
                        style={{ marginRight: 8 }}
                    >
                        Mostrar materias
                    </Button>
                </span>
            ),
        },
    ];



    const handleTableChange = (pagination, filters, sorter, { currentDataSource }) => {
        onFilteredDataChange(currentDataSource);
        
        const appliedFilters = {};
        Object.keys(filters).forEach((key) => {
            if (filters[key] && filters[key].length > 0) {
                appliedFilters[key] = filters[key];
            }
        });
        onFiltersChange(appliedFilters);
    };


    return ( 
        <Table 
            columns={columns} 
            dataSource={careers} 
            rowKey="_id" 
            scroll={{ y: 400 }} 
            onChange={handleTableChange}
            rowClassName={tema.valor === 'oscuro' ? 'dark-row' : ''}
            className={tema.valor === 'oscuro' ? 'dark-table' : ''}
        />)
};

export default CareersTable;
