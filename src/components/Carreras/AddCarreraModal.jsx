import React, { useEffect } from 'react';
import { Modal, Form, Input } from 'antd';

const AddCarreraModal = ({ visible, onCreate, onCancel, currentCareer }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (currentCareer) {
            form.setFieldsValue({
                ...currentCareer,
                name: currentCareer.name
            });
            console.log(currentCareer);
        } else {
            form.resetFields();
        }
    }, [currentCareer, form]);

    return (
        <Modal
            visible={visible}
            title={currentCareer ? "Editar Carrera" : "Agregar Nuevo Carrera"}
            okText={currentCareer ? "Actualizar" : "Crear"}
            cancelText="Cancelar"
            onCancel={onCancel}
            onOk={() => {
                form
                    .validateFields()
                    .then((values) => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch((info) => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
            >
                <Form.Item
                    name="name"
                    label="Nombre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre!' }]}
                >
                    <Input />
                </Form.Item>
                

                
                

                

            </Form>
        </Modal>
    );
};

export default AddCarreraModal;
