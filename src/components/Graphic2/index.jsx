import React from 'react';
import { Pie } from 'react-chartjs-2';
import { Chart as ChartJS, Title, Tooltip, Legend, ArcElement } from 'chart.js';

ChartJS.register(Title, Tooltip, Legend, ArcElement);

const Graphic2 = ({ data }) => {
    const labels = Object.keys(data);
    const values = Object.values(data);

    const chartData = {
        labels: labels,
        datasets: [
            {
                label: 'Cantidad',
                data: values,
                backgroundColor: [
                    
                    '#6c6e89', 
                    '#4b4f6d', 
                    '#8b8ca1', 
                    '#a1a7b3', 
                    '#b4b9c3', 
                ],
                borderColor: '#ffdbf4', 
                borderWidth: 2,
            },
        ],
    };

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
                labels: {
                    color: '#666390',
                },
            },
            title: {
                display: true,
                text: 'Alumnos por Carrera', 
                font: {
                    size: 20,
                },
                color: '#666390', 
            },
            tooltip: {
                callbacks: {
                    label: (tooltipItem) => `Cantidad: ${tooltipItem.raw}`,
                },
            },
        },
    };

    return (
        <div style={{ width: '50%', margin: '0 auto', padding: '10px', borderRadius: '8px' }}>
            <Pie data={chartData} options={options} />
        </div>
    );
};

export default Graphic2;
