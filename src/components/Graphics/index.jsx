import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Chart as ChartJS, Title, Tooltip, Legend, BarElement, CategoryScale, LinearScale } from 'chart.js';

ChartJS.register(Title, Tooltip, Legend, BarElement, CategoryScale, LinearScale);

const Graphic1 = ({ data }) => {
    const labels = Object.keys(data);
    const values = Object.values(data);

    const chartData = {
        labels: labels,
        datasets: [
            {
                label: 'Cantidad',
                data: values,
                backgroundColor: '#6c6e89', 
                borderColor: '#4b4f6d', 
                borderWidth: 2,
            },
        ],
    };

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
                labels: {
                    color: '#666390',
                },
            },
            title: {
                display: true,
                text: 'Calificaciones en general',
                font: {
                    size: 20, 
                },
                color: '#666390',
            },
            tooltip: {
                callbacks: {
                    label: (tooltipItem) => `Cantidad: ${tooltipItem.raw}`,
                },
            },
        },
        scales: {
            x: {
                ticks: {
                    color: '#666390', 
                },
            },
            y: {
                ticks: {
                    color: '#666390',
                },
            },
        },
    };

    return (
        <div style={{ width: '80%', margin: '0 auto', padding: '10px', borderRadius: '8px' }}>
            <Bar data={chartData} options={options} />
        </div>
    );
};

export default Graphic1;
