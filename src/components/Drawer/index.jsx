import React, { useState, useEffect } from 'react';
import { Drawer, Avatar, Form, Input, Button, notification } from 'antd';
import { UserOutlined, MailOutlined, SaveOutlined, LockOutlined, LogoutOutlined } from '@ant-design/icons';
import { useAuth } from '../../hooks/useAuth';
import EditPasswordModal from "../ForgotPassword/EditPasswordModal.jsx";
import './Drawer.css';
import { useTema } from '../../context/TemaContext.jsx';

const DrawerComponent = () => {
    const tema = useTema();
    const { user, updateUser, logout } = useAuth();
    const [form] = Form.useForm();
    const [open, setOpen] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const showDrawer = () => {
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleModalOk = () => {
        setIsModalOpen(false);
    };

    const handleModalCancel = () => {
        setIsModalOpen(false);
    };

    useEffect(() => {
        if (user) {
            form.setFieldsValue({
                name: user.name,
                lastname: user.lastname,
                email: user.email,
            });
        }
    }, [user, form]);

    const onFinish = async (values) => {
        try {
            const updatedValues = {
                ...values,
                roles: ["servicios_escolares"]
            };
            await updateUser(updatedValues);
            notification.success({
                message: 'Usuario actualizado',
                description: 'La información del usuario ha sido actualizada correctamente.',
                placement: 'topRight',
            });
            onClose();
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al actualizar la información del usuario.',
                placement: 'topRight',
            });
        }
    };

    const inputStyle = {
        backgroundColor: tema.valor === 'oscuro' ? '#222' : '#fff',
        color: tema.valor === 'oscuro' ? '#fff' : '#000',
        borderColor: tema.valor === 'oscuro' ? '#555' : '#ccc',
    };

    return (
        <>
            <Avatar
                onClick={showDrawer}
                size={44}
                style={{ backgroundColor: '#666390', cursor: 'pointer' }}
                icon={<UserOutlined />}
            />

            <Drawer
                title={
                    <div className="drawer-header">
                        <Avatar
                            size={44}
                            icon={<UserOutlined />}
                            style={{ backgroundColor: '#b199dd', marginRight: 10 }}
                        />
                        <span>Información del Usuario</span>
                    </div>
                }
                onClose={onClose}
                open={open}
                headerStyle={{ 
                    backgroundColor: tema.valor === 'claro' ? '#b199dd' : "#333", 
                    color: '#fcf4fa' 
                }}
                bodyStyle={{ 
                    backgroundColor: tema.valor === 'claro' ? '#fcf4fa' : "#121212"
                }}
            >
                <Form
                    form={form}
                    layout="vertical"
                    onFinish={onFinish}
                >
                    <Form.Item
                        name="name"
                        label="Nombre"
                        rules={[{ required: true, message: 'Por favor ingrese su nombre' }]}
                    >
                        <Input
                            prefix={<UserOutlined />}
                            style={inputStyle}
                        />
                    </Form.Item>
                    <Form.Item
                        name="lastname"
                        label="Apellidos"
                        rules={[{ required: true, message: 'Por favor ingrese sus apellidos' }]}
                    >
                        <Input
                            prefix={<UserOutlined />}
                            style={inputStyle}
                        />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="Email"
                        rules={[{ required: true, message: 'Por favor ingrese su email', type: 'email' }]}
                    >
                        <Input
                            prefix={<MailOutlined />}
                            style={inputStyle}
                            disabled
                        />
                    </Form.Item>
                    <Button type="primary" htmlType="submit" className="save-button" icon={<SaveOutlined />}>
                        Guardar
                    </Button>
                </Form>
                <div className="form-buttons">
                    <Button type="default" onClick={showModal} className="edit-password-button" icon={<LockOutlined />}>
                        Editar Contraseña
                    </Button>
                    <Button type="default" onClick={() => logout()} className="logout-button" icon={<LogoutOutlined />}>
                        Cerrar Sesión
                    </Button>
                </div>
            </Drawer>

            <EditPasswordModal
                isModalOpen={isModalOpen}
                handleModalOk={handleModalOk}
                handleModalCancel={handleModalCancel}
            />
        </>
    );
};

export default DrawerComponent;
