import { structureUserData } from "./estructurarDatos/structureUserData";
import { structureCareerData } from "./estructurarDatos/structureCareerData";
import { structureMateriasData } from "./estructurarDatos/structureMateriasData";
import { structureGradeData } from "./estructurarDatos/structureGradeData";
import { structureStudentData } from "./estructurarDatos/structureStudentData";
import { structureAsistenciaData } from "./estructurarDatos/structureAsistenciaData";

export const structureData = (data, module) => {
    switch (module) {
        case 'users': return structureUserData(data);
        case 'careers': return structureCareerData(data);
        case 'materias': return structureMateriasData(data);
        case 'grades': return structureGradeData(data);
        case 'students': return structureStudentData(data);
        case 'asistencias': return structureAsistenciaData(data);
    }
}