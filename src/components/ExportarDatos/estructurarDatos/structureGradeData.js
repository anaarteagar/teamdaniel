export const structureGradeData = (grades) => {

    const fileName = 'datos_calificaciones';

    const headers = [
        { label: "Nombre", key: "nombre" },
        { label: "Parcial", key: "parcial" },
        { label: "Grado", key: "grado" },
        { label: "Materia", key: "materia" },
        { label: "Calificacion", key: "calificacion" }
    ];

    const data = grades.map(g => ({
        nombre: g.student[0].name || 'sin datos',
        parcial: g.parcial || 'sin datos',
        email: g.grado || 'sin datos',
        materia: g.grade[0].name || 'sin datos',
        calificacion: g.subject || 'sin datos'
    }));


    return [fileName, headers, data];
}
