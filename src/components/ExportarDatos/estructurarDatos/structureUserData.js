export const structureUserData = (users) => {

    const fileName = 'datos_usuarios';

    const headers = [
        { label: "Nombre", key: "nombre" },
        { label: "Apellido", key: "apellido" },
        { label: "Email", key: "email" },
        { label: "Rol", key: "rol" }
    ];

    const data = users.map(u => ({
        nombre: u.name || 'sin datos',
        apellido: u.lastname || 'sin datos',
        email: u.email || 'sin datos',
        rol: u.roles[0].name || 'sin datos'
    }));


    return [fileName, headers, data];

}
