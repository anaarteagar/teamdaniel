export const structureCareerData = (careers) => {

    const fileName = 'datos_carreras';

    const headers = [
        { label: "Nombre", key: "nombre" },
    ];

    const data = careers.map(c => ({
        nombre: c.name || 'sin datos'
    }));

    return [fileName, headers, data];
}
