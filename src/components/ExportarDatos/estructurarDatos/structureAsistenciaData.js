export const structureAsistenciaData = (asistencias) => {

    const fileName = 'datos_asistencia';

    const headers = [
        { label: "Estudiante", key: "estudiante" },
        { label: "Materia", key: "materia" },
        { label: "Profesor", key: "profesor" },
        { label: "Dia", key: "dia" },
        { label: "Asistencia", key: "asistencia" }
    ];

    const data = asistencias.map(a => ({
        estudiante: `${a.student[0].name} ${a.student[0].lastname}` || 'sin datos',
        materia: a.mateer[0].name || 'sin datos',
        profesor: `${a.professor[0].name} ${a.professor[0].lastname}` || 'sin datos',
        dia: new Date(a.dia).toLocaleDateString('es-ES') || 'sin datos',
        asistencia: a.asistencia ? 'Asistió' : 'No asistió'
    }));

    return [fileName, headers, data];
}
