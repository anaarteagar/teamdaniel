export const structureStudentData = (students) => {

    const fileName = 'datos_alumnos';

    const headers = [
        { label: "Nombre", key: "nombre" },
        { label: "Apellido", key: "apellido" },
        { label: "Curp", key: "curp" }
    ];

    const data = students.map(s => ({
        nombre: s.name || 'sin datos',
        apellido: s.lastname || 'sin datos',
        curp: s.CURP || 'sin datos'
    }));


    return [fileName, headers, data];
}
