export const structureMateriasData = (materias) => {

    const fileName = 'datos_materias';

    const headers = [
        { label: "Nombre", key: "nombre" },
        { label: "Profesores", key: "profesores" },
        { label: "Carreras", key: "carreras" }
    ];

    const data = materias.map(m => {
        // Unir todos los nombres de los profesores en una cadena, separados por comas
        const profesores = m.professor && m.professor.length > 0 
            ? m.professor.map(p => p.name).join(', ') 
            : 'sin datos';

        // Unir todos los nombres de las carreras en una cadena, separados por comas
        const carreras = m.career && m.career.length > 0 
            ? m.career.map(c => c.name).join(', ') 
            : 'sin datos';

        return {
            nombre: m.name || 'sin datos',
            profesores: profesores,
            carreras: carreras
        };
    });

    return [fileName, headers, data];
}
