import { CSVLink } from "react-csv";
import { DownOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu } from "antd";
import { structureData } from "./structureData";
import { exportToExcel } from "./formatosExportacion/exportToExcel";
import { exportToJSON } from "./formatosExportacion/exportToJson";

export const ExportDataMenu = ({ filteredData, module }) => {

    const [fileName, headers, data] = structureData( filteredData, module );

    const menu = (
        <Menu>
            <Menu.Item key="csv">
                <CSVLink
                    headers={headers}
                    data={data}
                    filename={`${fileName}.csv`}
                    style={{ color: 'inherit' }}
                >
                    CSV
                </CSVLink>
            </Menu.Item>
            <Menu.Item key="xlsx" onClick={() => exportToExcel(data, fileName)}>
                Excel
            </Menu.Item>
            <Menu.Item key="json" onClick={() => exportToJSON(data, fileName)}>
                JSON
            </Menu.Item>
        </Menu>
    );

    return (
        <Dropdown overlay={menu}>
            <Button type="primary" style={{ marginBottom: 16, marginLeft: 10 }}>
                Exportar datos <DownOutlined />
            </Button>
        </Dropdown>
    );
}