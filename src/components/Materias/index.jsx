import { Button, Popconfirm, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import React from "react";

const MateriaTable = ({ materias, onDelete, onEdit, onFilteredDataChange, onFiltersChange, tema }) => {

    const uniqueValues = (array, key) => {
        return [...new Set(array.map(item => item[key]).flat())];
    };

    const nameFilters = uniqueValues(materias, 'name').map(name => ({ text: name, value: name }));
    const professorFilters = uniqueValues(materias.flatMap(materia => materia.professor), 'name').map(prof => ({ text: prof, value: prof }));
    const careerFilters = uniqueValues(materias.flatMap(materia => materia.career), 'name').map(car => ({ text: car, value: car }));

    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'name',
            key: 'name',
            render: name => name || "La materia no tiene nombre",
            filters: nameFilters,
            onFilter: (value, record) => record.name.includes(value),
            filterSearch: true,
            width: '30%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Profesor',
            dataIndex: 'professor',
            key: 'professor[0].name',
            render: professor => (professor && professor.length > 0 ? professor.map(prof => prof.name).join(', ') : "La materia no tiene maestro"),
            filters: professorFilters,
            onFilter: (value, record) => record.professor.some(prof => prof.name.includes(value)),
            filterSearch: true,
            width: '30%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Carrera',
            dataIndex: 'career',
            key: 'career[0].name',
            render: career => (career && career.length > 0 ? career.map(car => car.name).join(', ') : "La materia no tiene carrera"),
            filters: careerFilters,
            onFilter: (value, record) => record.career.some(car => car.name.includes(value)),
            filterSearch: true,
            width: '30%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Acciones',
            key: 'actions',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: (text, record) => (
                <span>
                    <Button
                        type="primary"
                        icon={<EditOutlined />}
                        onClick={() => onEdit(record)}
                        style={{ marginRight: 8 }}
                    >
                        Editar
                    </Button>
                    <Popconfirm
                        title="¿Estás seguro de eliminar esta materia?"
                        onConfirm={() => onDelete(record._id)}
                        okText="Sí"
                        cancelText="No"
                        overlayClassName={tema.valor === 'oscuro' ? 'dark-popconfirm' : ''} 
                    >
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Eliminar
                        </Button>
                    </Popconfirm>
                </span>
            ),
        },
    ];


    
    const handleTableChange = (pagination, filters, sorter, { currentDataSource }) => {
        onFilteredDataChange(currentDataSource);
        
        const appliedFilters = {};
        Object.keys(filters).forEach((key) => {
            if (filters[key] && filters[key].length > 0) {
                appliedFilters[key] = filters[key];
            }
        });
        onFiltersChange(appliedFilters);
    };


    return (
        <Table 
            columns={columns} 
            dataSource={materias} 
            rowKey="_id" 
            scroll={{ y: 400 }} 
            onChange={handleTableChange}
            rowClassName={tema.valor === 'oscuro' ? 'dark-row' : ''}
            className={tema.valor === 'oscuro' ? 'dark-table' : ''}
        />)
};

export default MateriaTable;
