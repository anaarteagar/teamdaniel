import React, { useEffect, useState } from 'react';
import { Modal, Form, Input, Select } from 'antd';
import { profesoresServiceSystem } from '../../services/profesores';
import { carrerasServiceSystem } from '../../services/carreras';

const AddMateriaModal = ({ visible, onCreate, onCancel, currentMateria }) => {
    const [form] = Form.useForm();
    const [profesores, setProfesores] = useState([]);
    const [carreras, setCarreras] = useState([]);

    useEffect(() => {
        if (currentMateria) {
            const selectedProfessors = currentMateria.professor.map(p => p._id);
            const selectedCarreras = currentMateria.career.map(c => c._id);
            
            form.setFieldsValue({ 
                ...currentMateria, 
                professor: selectedProfessors,
                career: selectedCarreras
            });
        } else {
            form.resetFields();
        }
        obtenerProfesores();
        obtenerCarreras();
    }, [currentMateria, form]);

    const obtenerProfesores = async () => {
        const profesoresObtenidos = await profesoresServiceSystem.getProfesores();
        setProfesores(profesoresObtenidos);
    }
    
    const obtenerCarreras = async () => {
        const carrerasObtenidas = await carrerasServiceSystem.getCarreras();
        setCarreras(carrerasObtenidas);
    }

    return (
        <Modal
            visible={visible}
            title={currentMateria ? "Editar Materia" : "Agregar Nueva Materia"}
            okText={currentMateria ? "Actualizar" : "Crear"}
            cancelText="Cancelar"
            onCancel={onCancel}
            onOk={() => {
                form
                    .validateFields()
                    .then((values) => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch((info) => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
            >
                <Form.Item
                    name="name"
                    label="Nombre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="professor"
                    label="Profesores"
                    rules={[{ required: true, message: 'Por favor seleccione un profesor!' }]}
                >
                    <Select
                        mode="multiple"
                        placeholder="Seleccione los profesores"
                    >
                        {
                            profesores.map(p => (
                                <Select.Option key={p._id} value={p._id}>{p.name} {p.lastname}</Select.Option>
                            ))
                        }
                    </Select>
                </Form.Item>

                <Form.Item
                    name="career"
                    label="Carreras"
                    rules={[{ required: true, message: 'Por favor seleccione una carrera!' }]}
                >
                    <Select
                        mode="multiple"
                        placeholder="Seleccione las carreras"
                    >
                        {
                            carreras.map(c => (
                                <Select.Option key={c._id} value={c._id}>{c.name}</Select.Option>
                            ))
                        }
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default AddMateriaModal;
