import { Button, Popconfirm, Table, DatePicker, Select } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import React from "react";

const { RangePicker } = DatePicker;
const { Option } = Select;

const AssistancesTable = ({ assistances, onDelete, onEdit, onFilteredDataChange, onFiltersChange, tema }) => {

    const uniqueValues = (array, key) => {
        return [...new Set(array.map(item => item[key][0].name))];
    };

    const studentFilters = uniqueValues(assistances, 'student').map(name => ({ text: name, value: name }));
    const professorFilters = uniqueValues(assistances, 'professor').map(name => ({ text: name, value: name }));
    const mateerFilters = uniqueValues(assistances, 'mateer').map(name => ({ text: name, value: name }));

    const attendanceFilters = [
        { text: 'Asistió', value: true },
        { text: 'No asistió', value: false },
    ];

    const columns = [
        {
            title: 'Estudiante',
            dataIndex: 'student',
            key: 'student',
            filters: studentFilters,
            onFilter: (value, record) => record.student[0].name.includes(value),
            render: student => `${student[0].name} ${student[0].lastname}`,
            filterSearch: true,
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Materia',
            dataIndex: 'mateer',
            key: 'mateer',
            filters: mateerFilters,
            onFilter: (value, record) => record.mateer[0].name.includes(value),
            render: mateer => mateer[0].name,
            filterSearch: true,
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Profesor',
            dataIndex: 'professor',
            key: 'professor',
            filters: professorFilters,
            onFilter: (value, record) => record.professor[0].name.includes(value),
            render: professor => `${professor[0].name} ${professor[0].lastname}`,
            filterSearch: true,
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Día',
            dataIndex: 'dia',
            key: 'dia',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: dia => dia ? new Date(dia).toLocaleDateString() : 'Sin fecha',
            filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
                <div style={{ padding: 8 }}>
                    <RangePicker size="small"
                        onChange={(dates) => {
                            setSelectedKeys(dates ? [dates] : []);
                            confirm();
                        }}
                        value={selectedKeys[0] ? selectedKeys[0] : []}
                    />
                </div>
            ),
            onFilter: (value, record) => {
                if (!value || value.length !== 2) return true;
                const [start, end] = value;
                const recordDate = new Date(record.dia);
                return recordDate >= start.toDate() && recordDate <= end.toDate();
            },
        },
        {
            title: 'Asistencia',
            dataIndex: 'asistencia',
            key: 'asistencia',
            filters: attendanceFilters,
            onFilter: (value, record) => record.asistencia === value,
            render: asistencia => asistencia ? "Asistió" : "No asistió",
            width: '20%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Acciones',
            key: 'actions',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: (text, record) => (
                <span>
                    <Button
                        type="primary"
                        icon={<EditOutlined />}
                        onClick={() => onEdit(record)}
                        style={{ marginRight: 8 }}
                    >
                        Editar
                    </Button>
                    <Popconfirm
                        title="¿Estás seguro de eliminar esta asistencia?"
                        onConfirm={() => onDelete(record._id)}
                        okText="Sí"
                        cancelText="No"
                        overlayClassName={tema.valor === 'oscuro' ? 'dark-popconfirm' : ''}
                    >
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Eliminar
                        </Button>
                    </Popconfirm>
                </span>
            ),
        },
    ];

    const handleTableChange = (pagination, filters, sorter, { currentDataSource }) => {
        onFilteredDataChange(currentDataSource);

        const appliedFilters = {};
        Object.keys(filters).forEach((key) => {
            if (filters[key] && filters[key].length > 0) {
                appliedFilters[key] = filters[key];
            }
        });
        onFiltersChange(appliedFilters);
    };

    return (
        <Table
            columns={columns}
            dataSource={assistances}
            rowKey="_id"
            onChange={handleTableChange}
            rowClassName={tema.valor === 'oscuro' ? 'dark-row' : ''}
            className={tema.valor === 'oscuro' ? 'dark-table' : ''}
        />
    );
};

export default AssistancesTable;
