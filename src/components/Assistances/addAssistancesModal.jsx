import React, { useEffect,useState } from 'react';
import { Modal, Form, Input, DatePicker, Select,Checkbox } from 'antd';
import dayjs from 'dayjs';

const AddAssistanceModal = ({ visible, onCreate, onCancel, currentAssistance, users, graders }) => {
    const [form] = Form.useForm();
    

    useEffect(() => {
        if (currentAssistance) {
            
            
            
            console.log(currentAssistance);
            form.setFieldsValue({
                student: currentAssistance.student[0]._id,
                mateer: currentAssistance.mateer[0]._id,
                asistencia: currentAssistance.asistencia,
                dia: dayjs(currentAssistance.dia)
            });
            
            
        } else {
            form.resetFields();
            form.setFieldsValue({ asistencia: false }); // Inicializa asistencia en false 
        }
    }, [currentAssistance, form]);

    return (
        <Modal
            visible={visible}
            title={currentAssistance ? "Editar Asistencia" : "Agregar Nueva Asistencia"}
            okText={currentAssistance ? "Actualizar" : "Crear"}
            cancelText="Cancelar"
            onCancel={onCancel}
            onOk={() => {
                form
                    .validateFields()
                    .then((values) => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch((info) => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form form={form} layout="vertical" name="form_in_modal">
                <Form.Item
                    name="student"
                    label="Estudiante"
                    rules={[{ required: true, message: 'Por favor seleccione el estudiante!' }]}
                >
                    <Select>
                        {users.map(user => (
                            <Select.Option key={user._id} value={user._id}>
                                {user.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="mateer"
                    label="Materia"
                    rules={[{ required: true, message: 'Por favor seleccione la materia!' }]}
                >
                    <Select>
                        {graders.map(grader => (
                            <Select.Option key={grader._id} value={grader._id}>
                                {grader.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="dia"
                    label="Día"
                    rules={[{ required: true, message: 'Por favor seleccione el día!' }]}
                >
                    <DatePicker  />
                </Form.Item>
                <Form.Item
                    name="asistencia"
                    valuePropName="checked"
                >
                    <Checkbox>Asistencia</Checkbox>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default AddAssistanceModal;
