import { Link } from "react-router-dom";
import { Layout, Menu, Button } from "antd";
import { BulbOutlined, BulbFilled } from '@ant-design/icons';
const { Header } = Layout;
import './NavBar.css';
import DrawerComponent from "../Drawer/index.jsx";
import Imagen from '../../assets/logo.png';
import { useAuth } from "../../hooks/useAuth";
import { useTema, useTemaDispatch } from "../../context/TemaContext.jsx";

const NavBar = () => {
    const { isProfessor, isServiceSchool, isAlumno } = useAuth();
    const tema = useTema();
    const dispatch = useTemaDispatch();

    const tabNames = ["Home"];
    if (isServiceSchool) {
        tabNames.push("Users", "Carreras", "Materias", "Calificaciones", "Students","Asistencias", "Graphics");
    } else if (isProfessor || isAlumno) {
        tabNames.push("Calificaciones");
    }
    if (isProfessor) {
        tabNames.push("Asistencias");
    }

    const items = tabNames.map((name, index) => ({
        key: index,
        label: name,
        url: index === 0 ? '/' : `/${name.toLowerCase()}`,
    }));

    const themeIcon = tema.valor === 'claro' ? <BulbOutlined /> : <BulbFilled />;

    return (
        <Header className="header-content" style={{
            backgroundColor: tema.valor === 'claro' ? "#666390" : '#121212' 
        }}>
            <div className="logo-container">
                <Link to="/">
                    <img src={Imagen} alt="logo" className="logo" />
                </Link>
            </div>
            <Menu
                mode="horizontal"
                defaultSelectedKeys={['0']}
                style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    flex: 1,
                    minWidth: 0,
                    marginRight: "20px"
                }}
                className="menu"
            >
                {items.map(item => (
                    <Menu.Item key={item.key} className="menu-item">
                        <Link style={{ color: "#fcf4fa" }} to={item.url}>{item.label}</Link>
                    </Menu.Item>
                ))}
            </Menu>
            <Button
                type="primary"
                icon={themeIcon}
                onClick={() => dispatch({ type: 'cambiar' })}
                style={{ marginRight: '10px' }}
            >
                Cambiar Tema
            </Button>
            <DrawerComponent />
        </Header>
    );
};

export default NavBar;
