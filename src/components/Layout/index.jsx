
import {Col, Row} from "antd";
import './LayoutComponent.css'
import { useTema } from "../../context/TemaContext";


const LayoutComponent = ({leftColSize, rightColSize, leftContent, rightContent}) => {
    
    const tema = useTema();
    
    // console.log('LeftColSize:', leftColSize);
    return (
        <div className="Layout-container">
            <Row>
                <Col xs={leftColSize.xs} sm={leftColSize.sm} md={leftColSize.md} lg={leftColSize.lg}>
                    <div className={ tema.valor === 'claro' ? "content-left" : "content-left-oscuro" }>
                        {leftContent}
                    </div>
                </Col>
                <Col xs={rightColSize.xs} sm={rightColSize.sm} md={rightColSize.md} lg={rightColSize.lg}>
                    <div className={ tema.valor === 'claro' ? "content-right" : "content-right-oscuro" }>
                        {rightContent}
                    </div>
                </Col>
            </Row>
        </div>

    )
}
export default LayoutComponent;