import React, { useEffect } from 'react';
import { Modal, Form, Input } from 'antd';

const AddStudentModal = ({ visible, onCreate, onCancel, currentStudent }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (currentStudent) {
            form.setFieldsValue(currentStudent);
        } else {
            form.resetFields();
        }
    }, [currentStudent, form]);
    const validateCURP = (_, value) => {
        const curpRegex = /^[A-Z]{4}\d{6}[H,M][A-Z]{5}[A-Z0-9]\d$/;
        if (value && !curpRegex.test(value)) {
            return Promise.reject(new Error('Por favor ingrese una CURP válida'));
        }
        return Promise.resolve();
    }
    return (
        <Modal
            visible={visible}
            title={currentStudent ? "Editar Estudiante" : "Agregar Nuevo Estudiante"}
            okText={currentStudent ? "Actualizar" : "Crear"}
            cancelText="Cancelar"
            onCancel={onCancel}
            onOk={() => {
                form
                    .validateFields()
                    .then((values) => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch((info) => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
            >
                <Form.Item
                    name="name"
                    label="Nombre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre!' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="lastname"
                    label="Apellido"
                    rules={[{ required: true, message: 'Por favor ingrese el apellido!' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="CURP"
                    label="CURP"
                    rules={[{ required: true, message: 'Por favor ingrese el CURP!' }, { validator: validateCURP }]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default AddStudentModal;