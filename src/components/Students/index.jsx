import React from 'react';
import { Table, Button, Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import '../../pages/Students/Students.css';

const StudentsTable = ({ students, onDelete, onEdit, onFilteredDataChange, onFiltersChange, tema }) => {

    const uniqueValues = (array, key) => {
        return [...new Set(array.map(item => item[key]))];
    };

    const nameFilters = uniqueValues(students, 'name').map(name => ({ text: name, value: name }));
    const lastnameFilters = uniqueValues(students, 'lastname').map(lastname => ({ text: lastname, value: lastname }));
    const curpFilters = uniqueValues(students, 'CURP').map(curp => ({ text: curp, value: curp }));

    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'name',
            key: 'name',
            filters: nameFilters,
            onFilter: (value, record) => record.name.includes(value),
            filterSearch: true,
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Apellido',
            dataIndex: 'lastname',
            key: 'lastname',
            filters: lastnameFilters,
            onFilter: (value, record) => record.lastname.includes(value),
            filterSearch: true,
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Curp',
            dataIndex: 'CURP',
            key: 'CURP',
            filters: curpFilters,
            onFilter: (value, record) => record.CURP.includes(value),
            filterSearch: true,
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Acciones',
            key: 'actions',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: (text, record) => (
                <span>
                    <Button
                        type="primary"
                        icon={<EditOutlined />}
                        onClick={() => onEdit(record)}
                        style={{ marginRight: 8 }}
                    >
                        Editar
                    </Button>
                    <Popconfirm
                        title="¿Estás seguro de eliminar este estudiante?"
                        onConfirm={() => onDelete(record._id)}
                        okText="Sí"
                        cancelText="No"
                        overlayClassName={tema.valor === 'oscuro' ? 'dark-popconfirm' : ''} 
                    >
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Eliminar
                        </Button>
                    </Popconfirm>
                </span>
            ),
        },
    ];

    const handleTableChange = (pagination, filters, sorter, { currentDataSource }) => {
        onFilteredDataChange(currentDataSource);
        
        const appliedFilters = {};
        Object.keys(filters).forEach((key) => {
            if (filters[key] && filters[key].length > 0) {
                appliedFilters[key] = filters[key];
            }
        });
        onFiltersChange(appliedFilters);
    };

    return (
        <div style={{ height: 900 }}>
            <Table 
                columns={columns} 
                dataSource={students} 
                rowKey="_id" 
                onChange={handleTableChange} 
                rowClassName={tema.valor === 'oscuro' ? 'dark-row' : ''}
                className={tema.valor === 'oscuro' ? 'dark-table' : ''}
            />
        </div>
    )
};

export default StudentsTable;
