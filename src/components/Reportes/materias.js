import { jsPDF } from 'jspdf'
import 'jspdf-autotable'
import Imagen from '../../assets/logo.png';

export const generatePDFMaterias = (usuario, datos, appliedFilters, valorBusqueda) => {

    try {
        const doc = new jsPDF();

        // Obtener la fecha y la hora actual
        const fecha = new Date();
        const fechaFormateada = `${fecha.getDate()}/${fecha.getMonth() + 1}/${fecha.getFullYear()}`;
        const horaFormateada = `${fecha.getHours()}:${fecha.getMinutes()}:${fecha.getSeconds()}`;


        doc.addImage(Imagen, "PNG", 15, 10, 30, 30);
        doc.setFontSize(20);
        doc.text('Reporte de Materias', 60, 25);

        // Crear tabla de datos generales
        const columnasGenerales = ['Dato', 'Valor'];
        const datosGenerales = [
            ['Usuario que genero el reporte', usuario],
            ['Fecha', fechaFormateada],
            ['Hora', horaFormateada]
        ];

        doc.autoTable({
            startY: 50,
            head: [columnasGenerales],
            body: datosGenerales
        });


        // Crear tabla de filtros
        const columnasFiltros = ['Filtro', 'Valor'];

        const datosFiltros = [
            ['Busqueda', valorBusqueda === '' ? 'No aplicado' : valorBusqueda],
            ['Nombre', appliedFilters.name || 'No aplicado'],
            ['Profesor', appliedFilters['professor[0].name'] || 'No aplicado'],
            ['Carrera', appliedFilters['career[0].name'] || 'No aplicado']
        ];

        doc.autoTable({
            startY: 90,
            head: [columnasFiltros],
            body: datosFiltros
        });

        //Crear tabla para los datos
        const columns = ['Nombre', 'Profesor', 'Carrera'];
        const data = [];
        console.log(datos)
        datos.forEach(materia => {

            const profesores = materia.professor.map(p => p.name);
            const carreras = materia.career.map(c => c.name);

            data.push([
                materia.name || 'sin datos',
                profesores || 'sin datos',
                carreras || 'sin datos'
            ])
        });

        doc.autoTable({
            startY: 150,
            head: [columns],
            body: data
        });

        // Formatear la fecha para el nombre del archivo
        const fechaArchivo = `${fecha.getDate()}-${fecha.getMonth() + 1}-${fecha.getFullYear()}`;

        // Guardar el pdf con la fecha
        doc.save(`Reporte_de_materias_${fechaArchivo}.pdf`);
    } catch (error) {
        alert('Se produjo un error');
    }
}