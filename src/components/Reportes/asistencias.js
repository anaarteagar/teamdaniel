import { jsPDF } from 'jspdf'
import 'jspdf-autotable'
import Imagen from '../../assets/logo.png';

export const generatePDFAssistances = (usuario, datos, appliedFilters, valorBusqueda) => {

    try {
        const doc = new jsPDF();

        console.log(datos);
        console.log(appliedFilters)

        // Obtener la fecha y la hora actual
        const fecha = new Date();
        const fechaFormateada = `${fecha.getDate()}/${fecha.getMonth() + 1}/${fecha.getFullYear()}`;
        const horaFormateada = `${fecha.getHours()}:${fecha.getMinutes()}:${fecha.getSeconds()}`;


        doc.addImage(Imagen, "PNG", 15, 10, 30, 30);
        doc.setFontSize(20);
        doc.text('Reporte de Estudiantes', 60, 25);

        // Crear tabla de datos generales
        const columnasGenerales = ['Dato', 'Valor'];
        const datosGenerales = [
            ['Usuario que genero el reporte', usuario],
            ['Fecha', fechaFormateada],
            ['Hora', horaFormateada]
        ];

        doc.autoTable({
            startY: 50,
            head: [columnasGenerales],
            body: datosGenerales
        });


        // Crear tabla de filtros
        const columnasFiltros = ['Filtro', 'Valor'];

        const datosFiltros = [
            ['Busqueda', valorBusqueda || 'No aplicado'],
            ['Estudiante', (appliedFilters.student && appliedFilters.student[0]) || 'No aplicado'],
            ['Materia', (appliedFilters.mateer && appliedFilters.mateer[0]) || 'No aplicado'],
            ['Profesor', (appliedFilters.professor && appliedFilters.professor[0]) || 'No aplicado'],
            ['Día', 
                (appliedFilters.dia && appliedFilters.dia[0].length === 2
                    ? `${new Date(appliedFilters.dia[0][0]).toLocaleDateString()} - ${new Date(appliedFilters.dia[0][1]).toLocaleDateString()}`
                    : 'No aplicado')
            ],
            ['Asistencia', (appliedFilters.asistencia !== undefined ? (appliedFilters.asistencia ? 'Asistió' : 'No asistió') : 'No aplicado')]
        ];

        doc.autoTable({
            startY: 90,
            head: [columnasFiltros],
            body: datosFiltros
        });

        //Crear tabla para los datos
        const columns = ['Nombre', 'Materia','Profesor','Dia','Asistencia'];
        const data = [];
        
        datos.forEach(assistance => {
            var dia = new Date(assistance.dia)
            data.push([
                assistance.student[0].name || 'sin datos',
                assistance.mateer[0].name || 'sin datos',
                assistance.professor[0].name || 'sin datos',
                `${dia.getDate()}/${dia.getMonth() + 1}/${dia.getFullYear()}`|| 'sin datos',
                (assistance.dia ? 'Asistio' : 'No Asistio') || 'sin datos',
            ])
        });

        doc.autoTable({
            startY: 150,
            head: [columns],
            body: data
        });

        // Formatear la fecha para el nombre del archivo
        const fechaArchivo = `${fecha.getDate()}-${fecha.getMonth() + 1}-${fecha.getFullYear()}`;

        // Guardar el pdf con la fecha
        doc.save(`Reporte_de_asistencias_${fechaArchivo}.pdf`);
    } catch (error) {
        alert('Se produjo un error');
    }
}