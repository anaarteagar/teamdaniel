import { jsPDF } from 'jspdf'
import 'jspdf-autotable'
import Imagen from '../../assets/logo.png';

export const generatePDFCarreras = (usuario, datos, appliedFilters, valorBusqueda) => {

    const doc = new jsPDF();

    console.log(appliedFilters);

    // Obtener la fecha y la hora actual
    const fecha = new Date();
    const fechaFormateada = `${fecha.getDate()}/${fecha.getMonth() + 1}/${fecha.getFullYear()}`;
    const horaFormateada = `${fecha.getHours()}:${fecha.getMinutes()}:${fecha.getSeconds()}`;


    doc.addImage(Imagen, "PNG", 15, 10, 30, 30);
    doc.setFontSize(20);
    doc.text('Reporte de Carreras', 60, 25);

    // Crear tabla de datos generales
    const columnasGenerales = ['Dato', 'Valor'];
    const datosGenerales = [
        ['Usuario que genero el reporte', usuario],
        ['Fecha', fechaFormateada],
        ['Hora', horaFormateada]
    ];

    doc.autoTable({
        startY: 50,
        head: [columnasGenerales],
        body: datosGenerales
    });


    // Crear tabla de filtros
    const columnasFiltros = ['Filtro', 'Valor'];

    const datosFiltros = [
        ['Busqueda', valorBusqueda==='' ? 'No aplicado' : valorBusqueda],
        ['Nombre', appliedFilters.name || 'No aplicado']
    ];

    doc.autoTable({
        startY: 90,
        head: [columnasFiltros],
        body: datosFiltros
    });

    //Crear tabla para los datos
    const columns = ['Nombre'];
    const data = [];
    datos.forEach(carrera => {
        data.push([
            carrera.name || 'sin datos'
        ])
    });
    
    doc.autoTable({
        startY: 130,
        head: [columns],
        body: data
    });

    // Formatear la fecha para el nombre del archivo
    const fechaArchivo = `${fecha.getDate()}-${fecha.getMonth() + 1}-${fecha.getFullYear()}`;

    // Guardar el pdf con la fecha
    doc.save(`Reporte_de_carreras_${fechaArchivo}.pdf`);
}