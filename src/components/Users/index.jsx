import { Button, Popconfirm, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import React from "react";
import './usersTable.css';

const UsersTable = ({ users, onDelete, onEdit, onFilteredDataChange, onFiltersChange, tema }) => {

    const uniqueValues = (array, key) => {
        return [...new Set(array.map(item => item[key]))];
    };

    const nameFilters = uniqueValues(users, 'name').map(name => ({ text: name, value: name }));
    const lastnameFilters = uniqueValues(users, 'lastname').map(lastname => ({ text: lastname, value: lastname }));
    const emailFilters = uniqueValues(users, 'email').map(email => ({ text: email, value: email }));
    const rolesFilters = uniqueValues(users.flatMap(user => user.roles), 'name').map(role => ({ text: role, value: role }));

    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'name',
            key: 'name',
            render: name => name || "El usuario no tiene nombre",
            filters: nameFilters,
            onFilter: (value, record) => record.name.includes(value),
            filterSearch: true,
            width: '20%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '', // Condicional para clase personalizada
        },
        {
            title: 'Apellido',
            dataIndex: 'lastname',
            key: 'lastname',
            render: lastname => lastname || "El usuario no tiene apellido",
            filters: lastnameFilters,
            onFilter: (value, record) => record.lastname.includes(value),
            filterSearch: true,
            width: '20%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '', // Condicional para clase personalizada
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            render: email => email || "El usuario no tiene correo electrónico",
            filters: emailFilters,
            onFilter: (value, record) => record.email.includes(value),
            filterSearch: true,
            width: '20%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '', // Condicional para clase personalizada
        },
        {
            title: 'Rol',
            dataIndex: 'roles',
            key: 'roles[0].name',
            render: roles => (roles && roles.length > 0 ? roles.map(role => role.name).join(', ') : "El usuario no tiene roles"),
            filters: rolesFilters,
            onFilter: (value, record) => record.roles.some(role => role.name.includes(value)),
            filterSearch: true,
            width: '20%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '', // Condicional para clase personalizada
        },
        {
            title: 'Acciones',
            key: 'actions',
            render: (text, record) => (
                <span>
                    <Button
                        type="primary"
                        icon={<EditOutlined />}
                        onClick={() => onEdit(record)}
                        style={{ marginRight: 8 }}
                    >
                        Editar
                    </Button>
                    <Popconfirm
                        title="¿Estás seguro de eliminar este usuario?"
                        onConfirm={() => onDelete(record._id)}
                        okText="Sí"
                        cancelText="No"
                        overlayClassName={tema.valor === 'oscuro' ? 'dark-popconfirm' : ''} // Aplicar estilo de Popconfirm en modo oscuro
                    >
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Eliminar
                        </Button>
                    </Popconfirm>
                </span>
            ),
            className: tema.valor === 'oscuro' ? 'dark-column' : '', // Condicional para clase personalizada
        },
    ];

    const handleTableChange = (pagination, filters, sorter, { currentDataSource }) => {
        onFilteredDataChange(currentDataSource);
        const appliedFilters = {};
        Object.keys(filters).forEach((key) => {
            if (filters[key] && filters[key].length > 0) {
                appliedFilters[key] = filters[key];
            }
        });

        onFiltersChange(appliedFilters);
    };

    return (
        <Table
            columns={columns}
            dataSource={users}
            rowKey="_id"
            scroll={{ y: 400 }}
            onChange={handleTableChange}
            rowClassName={tema.valor === 'oscuro' ? 'dark-row' : ''}
            className={tema.valor === 'oscuro' ? 'dark-table' : ''}
        />
    );
};

export default UsersTable;
