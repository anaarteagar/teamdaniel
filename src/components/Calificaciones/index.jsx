import { Button, Popconfirm, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import React from "react";
import { useAuth } from "../../hooks/useAuth";

const CalificacionesTable = ({ calificaciones, onDelete, onEdit, onFilteredDataChange, onFiltersChange, tema }) => {
    const { isProfessor, isServiceSchool } = useAuth();

    const uniqueValues = (array, key) => {
        return [...new Set(array.flatMap(item => item[key].map(subItem => subItem.name)))];
    };

    const studentFilters = uniqueValues(calificaciones, 'student').map(name => ({ text: name, value: name }));
    const gradeFilters = uniqueValues(calificaciones, 'grade').map(name => ({ text: name, value: name }));


    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'student',
            key: 'student',
            render: student => (student && student.length > 0 ? student.map(stu => stu.name).join(', ') : "La calificación no tiene estudiante"),
            filters: studentFilters,
            onFilter: (value, record) => record.student.some(stu => stu.name.includes(value)),
            filterSearch: true,
            width: '40%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Parcial',
            dataIndex: 'parcial',
            key: 'parcial',
            filters: [
                { text: '1', value: 1 },
                { text: '2', value: 2 },
            ],
            onFilter: (value, record) => record.parcial === value,
            filterSearch: true,
            width: '40%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Grado',
            dataIndex: 'grado',
            key: 'grado',
            filters: Array.from({ length: 11 }, (_, i) => ({ text: (i + 1).toString(), value: (i + 1).toString() })),
            onFilter: (value, record) => record.grado.toString() === value,
            filterSearch: true,
            width: '40%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Materia',
            dataIndex: 'grade',
            key: 'grade',
            render: grade => (grade && grade.length > 0 ? grade.map(gr => gr.name).join(', ') : "La calificación no tiene materia"),
            filters: gradeFilters,
            onFilter: (value, record) => record.grade.some(gr => gr.name.includes(value)),
            filterSearch: true,
            width: '40%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Calificación',
            dataIndex: 'subject',
            key: 'subject',
            filters: Array.from({ length: 10 }, (_, i) => ({ text: (i + 1).toString(), value: (i + 1).toString() })),
            onFilter: (value, record) => record.subject.toString() === value,
            filterSearch: true,
            width: '40%',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
        },
        {
            title: 'Puntuación final',
            dataIndex: 'finalScore',
            key: 'finalScore',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: score => (score ? score : "La calificación no tiene puntuación final")
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: score => (score ? score : "La calificación no tiene status")
        },
        ...(isProfessor || isServiceSchool ? [{
            title: 'Acciones',
            key: 'actions',
            className: tema.valor === 'oscuro' ? 'dark-column' : '',
            render: (text, record) => (
                <span>
                    <Button
                        type="primary"
                        icon={<EditOutlined />}
                        onClick={() => onEdit(record)}
                        style={{ marginRight: 8 }}
                    >
                        Editar
                    </Button>
                    <Popconfirm
                        title="¿Estás seguro de eliminar esta calificación?"
                        onConfirm={() => onDelete(record._id)}
                        okText="Sí"
                        cancelText="No"
                        overlayClassName={tema.valor === 'oscuro' ? 'dark-popconfirm' : ''}
                    >
                        <Button type="danger" icon={<DeleteOutlined />}>
                            Eliminar
                        </Button>
                    </Popconfirm>
                </span>
            ),
        }] : [])
    ];

    const handleTableChange = (pagination, filters, sorter, { currentDataSource }) => {
        onFilteredDataChange(currentDataSource);
        
        const appliedFilters = {};
        Object.keys(filters).forEach((key) => {
            if (filters[key] && filters[key].length > 0) {
                appliedFilters[key] = filters[key];
            }
        });
        onFiltersChange(appliedFilters);
    };

    return (
        <Table 
            columns={columns} 
            dataSource={calificaciones} 
            rowKey="_id" 
            pagination={{ pageSize: 6 }} 
            onChange={handleTableChange} 
            rowClassName={tema.valor === 'oscuro' ? 'dark-row' : ''}
            className={tema.valor === 'oscuro' ? 'dark-table' : ''}
        />
    )
};

export default CalificacionesTable;
