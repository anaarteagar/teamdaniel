import React, { useEffect, useState } from 'react';
import { Modal, Form, Select } from 'antd';

const { Option } = Select;

const CalificacionModal = ({ visible, onCancel, onCreate, materias, alumnos, currentCalification }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (currentCalification) {
            form.setFieldsValue({
                ...currentCalification,
                grade: currentCalification.grade[0]?.name,
                student: currentCalification.student[0]?._id,
            });
        } else {
            form.resetFields();
        }
    }, [currentCalification, form]);

    const handleMateriaChange = (materiaId) => {
        form.setFieldsValue({ grade: materiaId });
    };

    return (
        <Modal
            visible={visible}
            title={`Añadir calificación`}
            cancelText="Cerrar"
            onCancel={onCancel}
            okText={"Subir"}
            onOk={() => {
                form.validateFields()
                    .then((values) => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch((info) => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form form={form} layout="vertical" name="form_in_modal">
                <Form.Item
                    name="grade"
                    label="Materia"
                    rules={[{ required: true, message: 'Por favor selecciona una materia!' }]}
                >
                    <Select
                        placeholder="Selecciona una materia"
                        onChange={handleMateriaChange}
                        value={form.getFieldValue('grade')}
                    >
                        {materias.map((materia) => (
                            <Option key={materia._id} value={materia._id}>
                                {materia.name}
                            </Option>
                        ))}
                    </Select>
                </Form.Item>

                <Form.Item
                    name="student"
                    label="Alumno"
                    rules={[{ required: true, message: 'Por favor selecciona un alumno!' }]}
                >
                    <Select
                        placeholder="Selecciona un alumno"
                        value={form.getFieldValue('student')}
                    >
                        {alumnos.map((alumno) => (
                            <Option key={alumno._id} value={alumno._id}>
                                {alumno.name} {alumno.lastname}
                            </Option>
                        ))}
                    </Select>
                </Form.Item>

                <Form.Item
                    name="parcial"
                    label="Parcial"
                    rules={[{ required: true, message: 'Por favor selecciona un parcial!' }]}
                >
                    <Select placeholder="Selecciona un parcial">
                        <Option value="1">1</Option>
                        <Option value="2">2</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    name="grado"
                    label="Grado"
                    rules={[{ required: true, message: 'Por favor selecciona un grado!' }]}
                >
                    <Select placeholder="Selecciona un grado">
                        <Option value="1">1</Option>
                        <Option value="2">2</Option>
                        <Option value="3">3</Option>
                        <Option value="4">4</Option>
                        <Option value="5">5</Option>
                        <Option value="6">6</Option>
                        <Option value="7">7</Option>
                        <Option value="8">8</Option>
                        <Option value="9">9</Option>
                        <Option value="10">10</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    name="subject"
                    label="Calificación"
                    rules={[{ required: true, message: 'Por favor selecciona una calificación!' }]}
                >
                    <Select placeholder="Selecciona una calificación">
                        {[...Array(10).keys()].map((value) => (
                            <Option key={value + 1} value={value + 1}>
                                {value + 1}
                            </Option>
                        ))}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default CalificacionModal;
