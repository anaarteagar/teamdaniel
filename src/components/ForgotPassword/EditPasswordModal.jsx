import React from 'react';
import {Modal, Form, Input, Button, notification} from 'antd';
import auth from "../../services/auth.js";
const EditPasswordModal = ({ isModalOpen, handleModalOk, handleModalCancel }) => {
    const [form] = Form.useForm();

    const onFinish = async (values) => {
        const { password, confirmPassword } = values;
        if (password !== confirmPassword) {
            notification.error({
                message: 'Error',
                description: 'Las contraseñas no coinciden.',
                placement: 'topRight',
            });
            return;
        }
        else if (password.length < 8) {
            notification.error({
                message: 'Error',
                description: 'La contraseña debe tener al menos 8 caracteres.',
                placement: 'topRight',
            });
            return;
        }
        else if (!/[A-Z]/.test(password)) {
            notification.error({
                message: 'Error',
                description: 'La contraseña debe tener al menos una letra mayúscula.',
                placement: 'topRight',
            });
            return;
        }
        try {
            await auth.resetPasswordWithToken(password);
            console.log(auth.resetPasswordWithToken(password))
            notification.success({
                message: 'Contraseña actualizada',
                description: 'La contraseña ha sido actualizada correctamente.',
                placement: 'topRight',
            });
            handleModalOk();
            form.resetFields();
        } catch (error) {
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al actualizar la contraseña.',
                placement: 'topRight',
            });
        }
    };
    return (
        <Modal title="Editar Contraseña" open={isModalOpen} onCancel={handleModalCancel} footer={null}>
            <Form
                form={form}
                layout="vertical"
                onFinish={onFinish}
            >
                <Form.Item
                    name="password"
                    label="Nueva Contraseña"
                    rules={[{ required: true, message: 'Por favor ingrese su nueva contraseña' }]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    name="confirmPassword"
                    label="Confirmar Contraseña"
                    rules={[{ required: true, message: 'Por favor confirme su nueva contraseña' }]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Guardar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default EditPasswordModal;