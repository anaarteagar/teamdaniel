import { Form, Input, Button, Card, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useState } from "react";
import authService from '/src/services/auth';
import './ForgotPassword.css';
import {useNavigate} from "react-router-dom";

const ForgotPassword = () => {
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();
    const onFinish = async (values) => {
        setIsLoading(true);
        try {
            const response = await authService.resetPassword(values.email, values.password);
            console.log(response.data);
            if (response && response.data) {
                notification.success({
                    message: 'Contraseña actualizada',
                    description: 'Su contraseña ha sido actualizada exitosamente.',
                    placement: 'topRight',
                });
                navigate('/login');
            } else {
                notification.error({
                    message: 'Error',
                    description: 'Hubo un problema al actualizar la contraseña.',
                    placement: 'topRight',
                });
            }
        } catch (error) {
            console.error('Error during password reset:', error);
            notification.error({
                message: 'Error',
                description: 'Hubo un problema al actualizar la contraseña.',
                placement: 'topRight',
            });
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <Card
            title="Restablecer contraseña"
            bordered={false}
            className='responsive-card'
        >
            <Form
                name="forgot-password"
                className='forgot-password-form'
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Por favor ingrese su email' }]}
                >
                    <Input prefix={<UserOutlined />} placeholder='Email' />
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Por favor ingrese su nueva contraseña' }]}
                >
                    <Input.Password prefix={<LockOutlined />} placeholder='Nueva Contraseña' />
                </Form.Item>

                <Form.Item>
                    <Button type='primary' htmlType='submit' className='forgot-password-form-button' loading={isLoading}>
                        Restablecer Contraseña
                    </Button>
                </Form.Item>
            </Form>
        </Card>
    );
};

export default ForgotPassword;