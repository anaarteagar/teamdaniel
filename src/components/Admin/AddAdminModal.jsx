import React, { useEffect } from 'react';
import { Modal, Form, Input, Select } from 'antd';
import './addAdminModal.css'; // Importa los estilos CSS

const AddAdminModal = ({ visible, onCreate, onCancel, currentAdmin, tema }) => {
    const [form] = Form.useForm();


    useEffect(() => {
        if (currentAdmin) {
            form.setFieldsValue({
                ...currentAdmin,
                roles: currentAdmin.roles[0]?.name
            });
        } else {
            form.resetFields();
        }
    }, [currentAdmin, form]);

    return (
        <Modal
            visible={visible}
            title={currentAdmin ? "Editar Administrador" : "Agregar Nuevo Administrador"}
            okText={currentAdmin ? "Actualizar" : "Crear"}
            cancelText="Cancelar"
            onCancel={onCancel}
            onOk={() => {
                form
                    .validateFields()
                    .then((values) => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch((info) => {
                        console.log('Validate Failed:', info);
                    });
            }}
            className={tema.valor === 'oscuro' ? 'dark-mode' : ''} // Aplica la clase condicionalmente
        >
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
            >
                <Form.Item
                    name="name"
                    label="Nombre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre!' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="lastname"
                    label="Apellido"
                    rules={[{ required: true, message: 'Por favor ingrese el apellido!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="email"
                    label="Email"
                    rules={[{ required: true, message: 'Por favor ingrese el email!' }]}
                >
                    <Input />
                </Form.Item>

                {!currentAdmin && (
                    <Form.Item
                        name="password"
                        label="Contraseña"
                        rules={[{ required: true, message: 'Por favor ingrese la contraseña!' }]}
                    >
                        <Input.Password />
                    </Form.Item>
                )}

                <Form.Item
                    name="roles"
                    label="Rol"
                    rules={[{ required: true, message: 'Por favor ingrese el rol!' }]}
                >
                    <Select 
                        placeholder="Escoja Rol"
                        disabled={!!currentAdmin} 
                    >
                        <Select.Option value="profesor">Profesor</Select.Option>
                        <Select.Option value="servicios_escolares">Servicios Escolares</Select.Option>
                    </Select>
                    
                </Form.Item>

            </Form>
        </Modal>
    );
};

export default AddAdminModal;
